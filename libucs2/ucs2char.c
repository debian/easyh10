/*
 *      UCS-2 implementation (common routines).
 *
 *      Copyright (c) 2005 Nyaochi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA, or visit
 * http://www.gnu.org/copyleft/gpl.html .
 *
 */

/* $Id: ucs2char.c,v 1.29 2006/07/08 19:58:08 nyaochi Exp $ */

#ifdef	HAVE_CONFIG_H
#include <config.h>
#endif/*HAVE_CONFIG_H*/

#include <os.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <ucs2char.h>

#define	COMP(a, b)	((a)>(b))-((a)<(b))


void *ucs2malloc(size_t size)
{
	return malloc(size);
}

void *ucs2realloc(void *ptr, size_t size)
{
	return realloc(ptr, size);
}

void ucs2free(void* str)
{
	free(str);
}

int is_ucs2surrogate(ucs2_char_t c)
{
	return (0xD800 <= c && c <= 0xDFFF);
}

int isucs2space(ucs2_char_t c)
{
	return ((0x0009 <= c && c <= 0x000D) || c == 0x0020);
}

int isucs2digit(ucs2_char_t c)
{
	return (0x0030 <= c && c <= 0x0039);
}

size_t ucs2len(const ucs2_char_t* string)
{
	size_t length = 0;
	while (*string++)	length++;
	return length;
}

ucs2_char_t* ucs2cpy(ucs2_char_t* dst, const ucs2_char_t* src)
{
	ucs2_char_t *p = dst;
	for (;;) {
		*p = *src;
		if (*src == 0) {
			break;
		}
		p++;
		src++;
	}
	return dst;
}

ucs2_char_t* ucs2cat(ucs2_char_t* dst, const ucs2_char_t* src)
{
	ucs2_char_t *p = dst;
	while (*p)	p++;
	ucs2cpy(p, src);
	return dst;
}

ucs2_char_t* ucs2ncpy(ucs2_char_t* dst, const ucs2_char_t* src, size_t count)
{
	size_t i;
	ucs2_char_t *p = dst;
	for (i = 0;i < count;i++) {
		*p = *src;
		if (!*src) {
			break;
		}
		p++;
		src++;
	}
	return dst;
}

ucs2_char_t* ucs2chr(const ucs2_char_t* string, ucs2_char_t c)
{
	ucs2_char_t* ret = 0;
	while (*string) {
		if (*string == c)	{
			ret = (ucs2_char_t*)string;
			break;
		}
		string++;
	}
	return ret;
}

ucs2_char_t* ucs2rchr(const ucs2_char_t* string, ucs2_char_t c)
{
	ucs2_char_t* ret = 0;
	while (*string) {
		if (*string == c) {
			ret = (ucs2_char_t*)string;
		}
		string++;
	}
	return ret;
}

ucs2_char_t* ucs2str(const ucs2_char_t* str, const ucs2_char_t* search)
{
	size_t length = ucs2len(search);
	while (*str) {
		if (memcmp(str, search, length*sizeof(ucs2_char_t)) == 0) {
			return (ucs2_char_t*)str;
		}
		str++;
	}
	return 0;
}

int ucs2cmp(const ucs2_char_t* x, const ucs2_char_t* y)
{
	while (*x && *y && *x == *y) {
		x++;
		y++;
	}
	return COMP(*x, *y);
}

int ucs2ncmp(const ucs2_char_t* x, const ucs2_char_t* y, size_t n)
{
	size_t i;

	for (i = 0;i < n-1;i++) {
		if (!*x || !*y || *x != *y) {
			break;
		}
		x++;
		y++;
	}
	return COMP(*x, *y);
}

int ucs2icmp(const ucs2_char_t* x, const ucs2_char_t* y)
{
	ucs2_char_t a, b;

	do {
		a = ucs2upper(*x);
		b = ucs2upper(*y);
		if (!*x || !*y) {
			break;
		}
		x++;
		y++;
	} while (a == b);
	return COMP(a, b);
}

ucs2_char_t* ucs2dup(const ucs2_char_t* src)
{
	ucs2_char_t* dst = (ucs2_char_t*)malloc((ucs2len(src)+1) * sizeof(ucs2_char_t));
	ucs2cpy(dst, src);
	return dst;
}

ucs2_char_t* ucs2ndup(const ucs2_char_t* src, size_t length)
{
	ucs2_char_t* dst = NULL;
	size_t src_length = ucs2len(src);
	if (length < src_length) {
		src_length = length;
	}
	dst = (ucs2_char_t*)malloc((src_length+1) * sizeof(ucs2_char_t));
	if (dst) {
		size_t i;
		for (i = 0;i < src_length;i++) {
			dst[i] = src[i];
		}
		dst[i] = 0;
	}
	return dst;
}

ucs2_char_t* ucs2strip(ucs2_char_t* str)
{
	if (str && *str) {
		ucs2_char_t* p = NULL;
		for (p = str + ucs2len(str) - 1;str <= p;p--) {
			if (!isucs2space(*p)) {
				*(p + 1) = 0;
				break;
			}
		}

		for (p = str;p < str + ucs2len(str);p++) {
			if (!isucs2space(*p)) {
				break;
			}
		}

		if (p != str) {
			ucs2_char_t *q = NULL;
			for (q = str;*p;p++,q++) {
				*q = *p;
			}
			*q = 0;
		}
	}
	return str;
}

ucs2_char_t ucs2lower(ucs2_char_t ch)
{
	/* iriver only converts what we call one-bytes characters. */
	/* We must limit the range of case conversion. */
	if (!(ch & 0xFF80)) {
		ch = tolower(ch);
	}
	return ch;
}

ucs2_char_t* ucs2lwr(ucs2_char_t* str)
{
	ucs2_char_t* p = str;
	while (*p) {
		*p = ucs2lower(*p);
		p++;
	}
	return str;
}

ucs2_char_t ucs2upper(ucs2_char_t ch)
{
	/* iriver only converts what we call one-bytes characters. */
	/* We must limit the range of case conversion. */
	if (!(ch & 0xFF80)) {
		ch = toupper(ch);
	}
	return ch;
}

ucs2_char_t* ucs2upr(ucs2_char_t* str)
{
	ucs2_char_t* p = str;
	while (*p) {
		*p = ucs2upper(*p);
		p++;
	}
	return str;
}

static void print_ucs2(const ucs2_char_t* str)
{
  while (*str) {
    fprintf(stderr, "0x%X %c ", *str);
    str++;
  }
  fprintf(stderr, "\n");
}

ucs2_char_t* mbsdupucs2(const char *mbstr)
{
	size_t ucs2_size = 	mbstoucs2(NULL, 0, mbstr, strlen(mbstr)) + 1;
	ucs2_char_t* dst = (ucs2_char_t*)malloc(ucs2_size * sizeof(ucs2_char_t));
	if (dst) {
		mbstoucs2(dst, ucs2_size * sizeof(ucs2_char_t), mbstr, strlen(mbstr)+1);
	}
	return dst;
}

ucs2_char_t* mbsdupucs2_music(const char *mbstr)
{
	size_t ucs2_size = 	mbstoucs2_music(NULL, 0, mbstr, strlen(mbstr)) + 1;
	ucs2_char_t* dst = (ucs2_char_t*)malloc(ucs2_size * sizeof(ucs2_char_t));
	if (dst) {
		mbstoucs2_music(dst, ucs2_size * sizeof(ucs2_char_t), mbstr, strlen(mbstr)+1);
	}
	return dst;
}

ucs2_char_t* utf8dupucs2(const char *utf8str)
{
	size_t ucs2_size = 	utf8toucs2(NULL, 0, utf8str, strlen(utf8str)) + 1;
	ucs2_char_t* dst = (ucs2_char_t*)malloc(ucs2_size * sizeof(ucs2_char_t));
	if (dst) {
		utf8toucs2(dst, ucs2_size * sizeof(ucs2_char_t), utf8str, strlen(utf8str)+1);
	}
	return dst;
}

char *ucs2dupmbs(const ucs2_char_t *ucs2str)
{
	size_t mbs_size = ucs2tombs(NULL, 0, ucs2str, ucs2len(ucs2str)) + 1;
	char* dst = (char *)malloc(mbs_size * sizeof(char));
	if (dst) {
		ucs2tombs(dst, mbs_size, ucs2str, ucs2len(ucs2str)+1);
	}
	return dst;
}

char *ucs2duputf8(const ucs2_char_t* ucs2str)
{
	size_t mbs_size = ucs2toutf8(NULL, 0, ucs2str, ucs2len(ucs2str)) + 1;
	char* dst = (char *)malloc(mbs_size * sizeof(char));
	if (dst) {
		ucs2toutf8(dst, mbs_size, ucs2str, ucs2len(ucs2str)+1);
	}
	return dst;
}


//#define TEST_RIGHTTOLEFT

#ifdef	TEST_RIGHTTOLEFT

ucs2_char_t* ucs2righttoleft_encode(ucs2_char_t* str)
{
	/* Requiement: we must not increase the length of a string. */
	if (str) {
		ucs2_char_t* p = str;
		ucs2_char_t* q = str + ucs2len(str) - 1;
		for (;p < q;p++, q--) {
			ucs2_char_t tmp = *p;
			*p = *q;
			*q = tmp;
		}
	}
	return str;
}

ucs2_char_t* ucs2righttoleft_decode(ucs2_char_t* str)
{
	/* Requiement: we must not increase the length of a string. */
	return ucs2righttoleft_encode(str);
}

#else

ucs2_char_t* ucs2righttoleft_encode(ucs2_char_t* str)
{
	/* Requiement: we must not increase the length of a string. */

	int revlength = 0;
	ucs2_char_t temp;
	ucs2_char_t* p = str;
	ucs2_char_t* pr = NULL;
	int length = 0;
	int i = 0;
	int flipmode = 0;
	
	if (str) { /* checking for null string, since the common functions don't */
		length = ucs2len(str);
		pr = ucs2dup(str);
	}
	while (p && *p && pr) {
				if (*p >= 0x0590 && *p <= 0x06FF) {
						*(pr+length-(1+i)) = *p; /* Copy text reversed */
						flipmode = 1; /* Setting flip mode to handle punctuation properly */
					}
					else if ((*p >= 0x0030 && *p <= 0x0039)||
						 (*p > 0x0040 && *p < 0x005A) || 
						 (*p > 0x0060 && *p < 0x007A) || !flipmode) { /* check for latin and numbers */
					while (*p && (!(*p >= 0x0590 && *p <= 0x06FF))) { /* Check for non Hebrew\Arabic */
						p++;
						i++;
						revlength++;
					}
					ucs2ncpy((pr+length-i),p-revlength,revlength); /* Copy English text as-is */
					revlength =0;
				} 
				else {
				if ((*p == 0x0028) || (*p == 0x0029) || 
					(*p == 0x005B) || (*p == 0x005D) || 
					(*p == 0x003C) || (*p == 0x003E) || 
					(*p == 0x007B) || (*p == 0x007D))
				{
				if ((*p & 0x000F) == 0x000B)  *(pr+length-(1+i)) = *p +2;
					else if ((*p & 0x000F) == 0x000D)  *(pr+length-(1+i)) = *p -2;
					else if ((*p & 0x000F) == 0x0008)  *(pr+length-(1+i)) = *p +1;
					else if ((*p & 0x000F) == 0x0009)  *(pr+length-(1+i)) = *p -1;
					else if ((*p & 0x000F) == 0x000C)  *(pr+length-(1+i)) = *p -2;
					else if ((*p & 0x000F) == 0x000E)  *(pr+length-(1+i)) = *p +2;
			}
			else *(pr+length-(1+i)) = *p; /* Handling punctuation */
		}
		if (*p) p++;
		i++;
	} 
	if (pr && str) ucs2cpy(str,pr);
	free(pr);
	return (str);
}

ucs2_char_t* ucs2righttoleft_decode(ucs2_char_t* str)
{
	/* Requiement: we must not increase the length of a string. */
	return ucs2righttoleft_encode(str);
}

#endif
