/*
 *      Tag and audio information retrieval.
 *
 *      Copyright (c) 2005 Nyaochi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA, or visit
 * http://www.gnu.org/copyleft/gpl.html .
 *
 */

/* $Id: getmediainfo.h,v 1.9 2005/08/25 17:08:59 nyaochi Exp $ */

#ifndef	__GETINFO_H__
#define	__GETINFO_H__

#ifdef	__cplusplus
extern "C" {
#endif/*__cplusplus*/

struct tag_media_info {
	ucs2_char_t *pathname;
	ucs2_char_t *filename;
	ucs2_char_t	*title;
	ucs2_char_t	*artist;
	ucs2_char_t	*album;
	ucs2_char_t	*genre;
	uint32_t	tracknumer;
	uint32_t	year;
	uint32_t	filesize;
	uint32_t	duration;
	uint32_t	samplerate;
	uint32_t	bitrate;
};
typedef struct tag_media_info media_info;

void getmediainfo_init(media_info* info);
void getmediainfo_finish(media_info* info);

int gettag_mp3(media_info* info, const ucs2_char_t *path, const ucs2_char_t *file);
int gettag_wma(media_info* info, const ucs2_char_t *path, const ucs2_char_t *file);
int gettag_wav(media_info* info, const ucs2_char_t *path, const ucs2_char_t *file);

int gettag_set_info(h10db_t* h10db, int index, const media_info* info);
int gettag_get_info(h10db_t* h10db, int index, media_info* info);
int gettag_force_order(h10db_t* h10db, int index, uint32_t order, uint32_t max_order);
ucs2_char_t* gettag_insert_ordinalnumber(const ucs2_char_t* str, uint32_t number, uint32_t max_number);

ucs2_char_t* gettag_setfilename(const ucs2_char_t* file);

#ifdef	__cplusplus
}
#endif/*__cplusplus*/

#endif/*__GETINFO_H__*/
