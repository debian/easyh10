/*
 *      Data serializer with byte-order consideration.
 *
 *      Copyright (c) 2005 Nyaochi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA, or visit
 * http://www.gnu.org/copyleft/gpl.html .
 *
 */

/* $Id: serialize.h,v 1.3 2005/05/28 21:53:51 nyaochi Exp $ */

#ifndef	__SERIALIZE_H__
#define	__SERIALIZE_H__

int serialize_uint16(struct bfile *bfp, uint16_t* val, int is_storing);
int serialize_uint32(struct bfile *bfp, uint32_t* val, int is_storing);
int serialize_ucs2le(struct bfile *bfp, ucs2_char_t** val, int is_storing);
int serialize_ucs2le_fixed(struct bfile *bfp, ucs2_char_t* val, size_t length, int is_storing);
int serialize_ucs2le_limited(struct bfile *bfp, ucs2_char_t** val, size_t length, int is_storing);

#endif/*__SERIALIZE_H__*/

