/*
 *      Common routines for tag and audio information retrieval.
 *
 *      Copyright (c) 2005 Nyaochi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA, or visit
 * http://www.gnu.org/copyleft/gpl.html .
 *
 */

/* $Id: getmediainfo.c,v 1.16 2005/08/25 17:08:59 nyaochi Exp $ */

#ifdef	HAVE_CONFIG_H
#include <config.h>
#endif/*HAVE_CONFIG_H*/

#include <os.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>
#include <ucs2char.h>
#include <h10db.h>
#include <getmediainfo.h>


static const ucs2_char_t ucs2cs_unknown_title[] = {'U','n','k','n','o','w','n',' ','T','i','t','l','e',0};
static const ucs2_char_t ucs2cs_unknown_artist[] = {'U','n','k','n','o','w','n',' ','A','r','t','i','s','t',0};
static const ucs2_char_t ucs2cs_unknown_album[] = {'U','n','k','n','o','w','n',' ','A','l','b','u','m',0};
static const ucs2_char_t ucs2cs_unknown_genre[] = {'U','n','k','n','o','w','n',' ','G','e','n','r','e',0};

/* I do want to rewite this function to use gettag_insert_ordinalnumber(), but
 * am taking a safety first until we release 1.0.
 */
int getinfo_set_title(
	h10db_t* h10db,
	int index,
	const ucs2_char_t* title,
	uint32_t tn,
	int tn_flags)
{
	int retval = 0;
	static const ucs2_char_t ucs2_empty[1] = {0};

	if (tn_flags == 0) {
		h10db_set_title(h10db, index, title ? title : ucs2_empty);
	} else if (tn_flags == -1) {
		size_t title_length = title ? ucs2len(title) : 0;
		ucs2_char_t *new_title = NULL, *track_number = NULL;
		char tracknumber[32];

		snprintf(tracknumber, sizeof(tracknumber)-1, "%02d ", tn);
		track_number = mbsdupucs2(tracknumber);
		if (!track_number) {
			return -1;
		}

		new_title = malloc(sizeof(ucs2_char_t) * (title_length+ucs2len(track_number)+1));
		if (new_title) {
			ucs2cpy(new_title, track_number);
			ucs2cat(new_title, title ? title : ucs2_empty);
			h10db_set_title(h10db, index, new_title);
			free(new_title);
		} else {
			h10db_set_title(h10db, index, ucs2_empty);
			retval = -1;
		}
		ucs2free(track_number);
	} else if (tn_flags == 1) {
		size_t title_length = title ? ucs2len(title) : 0;
		ucs2_char_t *new_title = malloc(sizeof(ucs2_char_t) * (title_length+1+1));
		if (new_title) {
			new_title[0] = (ucs2_char_t)0xD800 + tn;
			ucs2cpy(new_title+1, title ? title : ucs2_empty);
			h10db_set_title(h10db, index, new_title);
			free(new_title);
		} else {
			h10db_set_title(h10db, index, ucs2_empty);
			retval = -1;
		}
	} else if (tn_flags == 2) {
		size_t title_length = title ? ucs2len(title) : 0;
		ucs2_char_t *new_title = malloc(sizeof(ucs2_char_t) * (title_length+2+1));
		if (new_title) {
			new_title[0] = (ucs2_char_t)0xD800 + (tn & 0x07FF);
			new_title[1] = (ucs2_char_t)0xD800 + ((tn >> 11) & 0x07FF);
			ucs2cpy(new_title+2, title ? title : ucs2_empty);
			h10db_set_title(h10db, index, new_title);
			free(new_title);
		} else {
			h10db_set_title(h10db, index, ucs2_empty);
			retval = -1;
		}
	}

	return retval;
}

void getmediainfo_init(media_info* info)
{
	memset(info, 0, sizeof(*info));
}

void getmediainfo_finish(media_info* info)
{
	free(info->pathname);
	free(info->filename);
	free(info->title);
	free(info->artist);
	free(info->album);
	free(info->genre);
	getmediainfo_init(info);
}

int gettag_set_info(h10db_t* h10db, int index, const media_info* info)
{
	static const ucs2_char_t ucs2_empty[1] = {0};

	if (h10db && info) {
		ucs2strip(info->title);
		if (info->title && ucs2icmp(info->title, ucs2cs_unknown_title) != 0) {
			h10db_set_title(h10db, index, info->title);
		} else {
			h10db_set_title(h10db, index, info->filename);
		}
		ucs2strip(info->artist);
		if (info->artist && ucs2icmp(info->artist, ucs2cs_unknown_artist) != 0) {
			h10db_set_artist(h10db, index, info->artist);
		} else {
			h10db_set_artist(h10db, index, ucs2_empty);
		}
		ucs2strip(info->album);
		if (info->album && ucs2icmp(info->album, ucs2cs_unknown_album) != 0) {
			h10db_set_album(h10db, index, info->album);
		} else {
			h10db_set_album(h10db, index, ucs2_empty);
		}
		ucs2strip(info->genre);
		if (info->genre && ucs2icmp(info->genre, ucs2cs_unknown_genre) != 0) {
			h10db_set_genre(h10db, index, info->genre);
		} else {
			h10db_set_genre(h10db, index, ucs2_empty);
		}
		h10db_set_tracknumber(h10db, index, info->tracknumer);	/* This track number may be faked. */
		h10db_set_year(h10db, index, info->year);
		h10db_set_filesize(h10db, index, info->filesize);
		h10db_set_duration(h10db, index, info->duration);
		h10db_set_samplerate(h10db, index, info->samplerate);
		h10db_set_bitrate(h10db, index, info->bitrate);
		h10db_set_unknown5(h10db, index, info->tracknumer);		/* Set unknown5 to be the track number. */
		h10db_set_unknown6(h10db, index, ucs2_empty);
		h10db_set_unknown8(h10db, index, ucs2_empty);
	}

	return 0;
}

static ucs2_char_t* dupucs2(const ucs2_char_t* val)
{
	static const ucs2_char_t ucs2_empty[1] = {0};
	return ucs2dup(val ? val : ucs2_empty);
}

static ucs2_char_t* dupucs2strip(const ucs2_char_t* val)
{
	static const ucs2_char_t ucs2_empty[1] = {0};
	if (val) {
		while (0xD800 <= *val && *val < 0xDFFF) {
			val++;
		}
		return ucs2dup(val);
	} else {
		return ucs2dup(ucs2_empty);
	}
}

int gettag_get_info(h10db_t* h10db, int index, media_info* info)
{
	if (h10db && info) {
		info->pathname = dupucs2(h10db_get_filepath(h10db, index));
		info->filename = dupucs2(h10db_get_filename(h10db, index));
		info->title = dupucs2strip(h10db_get_title(h10db, index));	/* Strip track number. */
		info->artist = dupucs2strip(h10db_get_artist(h10db, index));
		info->album = dupucs2strip(h10db_get_album(h10db, index));
		info->genre = dupucs2strip(h10db_get_genre(h10db, index));
		info->tracknumer = h10db_get_tracknumber(h10db, index);	/* This will be overwritten by unknown5 below. */
		info->year = h10db_get_year(h10db, index);
		info->filesize = h10db_get_filesize(h10db, index);
		info->duration = h10db_get_duration(h10db, index);
		info->samplerate = h10db_get_samplerate(h10db, index);
		info->bitrate = h10db_get_bitrate(h10db, index);
		info->tracknumer = h10db_get_unknown5(h10db, index);
		/*h10db_get_unknown6(h10db, index, ucs2_empty);*/
		/*h10db_get_unknown8(h10db, index, ucs2_empty);*/
	}

	return 0;
}

int gettag_force_order(h10db_t* h10db, int index, uint32_t order, uint32_t max_order)
{
	ucs2_char_t* title = NULL;
	int flags = 0;

	if (max_order < 0) {
		flags = -1;
	} else if (max_order == 0) {
		flags = 0;
	} else if (max_order <= 2048) {
		flags = 1;
	} else {
		flags = 2;
	}

	h10db_set_tracknumber(h10db, index, order);
	title = dupucs2(h10db_get_title(h10db, index));
	getinfo_set_title(h10db, index, title, order, flags);
	free(title);

	return 0;
}

ucs2_char_t* gettag_insert_ordinalnumber(const ucs2_char_t* str, uint32_t number, uint32_t max_number)
{
	int flags = 0;
	static const ucs2_char_t ucs2_empty[1] = {0};

	if (max_number < 0) {
		flags = -1;
	} else if (max_number == 0) {
		flags = 0;
	} else if (max_number <= 2048) {
		flags = 1;
	} else {
		flags = 2;
	}

	if (flags == 0) {
		return ucs2dup(str ? str : ucs2_empty);
	} else if (flags == -1) {
		size_t length = str ? ucs2len(str) : 0;
		ucs2_char_t *new_str = NULL, *ordinal_number = NULL;
		char ordinalnumber[32];

		snprintf(ordinalnumber, sizeof(ordinalnumber)-1, "%02d ", number);
		ordinal_number = mbsdupucs2(ordinalnumber);
		if (!ordinal_number) {
			return ucs2dup(ucs2_empty);
		}

		new_str = ucs2malloc(sizeof(ucs2_char_t) * (length+ucs2len(ordinal_number)+1));
		if (new_str) {
			ucs2cpy(new_str, ordinal_number);
			ucs2cat(new_str, str ? str : ucs2_empty);
			ucs2free(ordinal_number);
			return new_str;
		} else {
			ucs2free(ordinal_number);
			return ucs2dup(ucs2_empty);
		}
	} else if (flags == 1) {
		size_t length = str ? ucs2len(str) : 0;
		ucs2_char_t *new_str = ucs2malloc(sizeof(ucs2_char_t) * (length+1+1));
		if (new_str) {
			new_str[0] = (ucs2_char_t)0xD800 + number;
			ucs2cpy(new_str+1, str ? str : ucs2_empty);
			return new_str;
		} else {
			return ucs2dup(ucs2_empty);
		}
	} else if (flags == 2) {
		size_t length = str ? ucs2len(str) : 0;
		ucs2_char_t *new_str = malloc(sizeof(ucs2_char_t) * (length+2+1));
		if (new_str) {
			new_str[0] = (ucs2_char_t)0xD800 + (number & 0x07FF);
			new_str[1] = (ucs2_char_t)0xD800 + ((number >> 11) & 0x07FF);
			ucs2cpy(new_str+2, str ? str : ucs2_empty);
			return new_str;
		} else {
			return ucs2dup(ucs2_empty);
		}
	}
	return ucs2dup(ucs2_empty);
}

ucs2_char_t* gettag_setfilename(const ucs2_char_t* file)
{
	ucs2_char_t *ucs2 = NULL;
	ucs2_char_t* filename = ucs2dup(file);
	if (filename) {
		ucs2_char_t *p = ucs2rchr(filename, '.');
		if (*p) {
			*p = 0;
		}
		ucs2 = ucs2dup(filename);
		free(filename);
	}
	return ucs2;
}
