/*
 *      File/path utility implemented with POSIX API.
 *
 *      Copyright (c) 2005 Nyaochi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA, or visit
 * http://www.gnu.org/copyleft/gpl.html .
 *
 */

/* $Id: filepathutil_posix.c,v 1.18 2005/11/05 13:43:51 nyaochi Exp $ */

#ifdef	HAVE_CONFIG_H
#include <config.h>
#endif/*HAVE_CONFIG_H*/

#include <os.h>

#include <dirent.h>
#include <limits.h>
#include <string.h>
#include <sys/stat.h>

#include "filepathutil.h"

ucs2_char_t *rel2abs(const ucs2_char_t *path, const ucs2_char_t *base, ucs2_char_t *ucs2_char_t, size_t size);

int find_file(const ucs2_char_t* path, int recursive, filepathutil_findfile_callback callback, void *instance)
{
	DIR *dirp = NULL;
	struct dirent *entp = NULL;
	ucs2_char_t	tmp[MAX_PATH];
	char mbs[MAX_PATH+1];

	ucs2cpy(tmp, path);
	filepathutil_removeslash(tmp);
	filepathutil_decode(tmp);
	ucs2tombs(mbs, MAX_PATH, tmp, ucs2len(tmp)+1);

	//printf("opendir: %s\n", mbs);
	dirp = opendir(mbs);
	if (dirp) {
		while (entp = readdir(dirp)) {
			ucs2_char_t filename[MAX_PATH+1];
			if (strcmp(entp->d_name, ".") == 0 || strcmp(entp->d_name, "..") == 0) {
				continue;
			}
			//printf("readdir: %s\n", entp->d_name);
			mbstoucs2(filename, MAX_PATH, entp->d_name, strlen(entp->d_name)+1);

			ucs2cpy(tmp, path);
			ucs2cat(tmp, filename);
			if (!ucs2stat_is_dir(tmp)){
				callback(instance, path, filename);
			}
		}
		closedir(dirp);
	}

	if (recursive) {
		ucs2cpy(tmp, path);
		filepathutil_removeslash(tmp);
		filepathutil_decode(tmp);
		ucs2tombs(mbs, MAX_PATH, tmp, ucs2len(tmp)+1);

		//printf("opendir2: %s\n", mbs);
		dirp = opendir(mbs);
		if (dirp) {
			while (entp = readdir(dirp)) {
				struct stat st;
				ucs2_char_t filename[MAX_PATH+1];
				mbstoucs2(filename, MAX_PATH, entp->d_name, strlen(entp->d_name)+1);

				//printf("readdir2: %s\n", entp->d_name);
				if (strcmp(entp->d_name, ".") == 0 || strcmp(entp->d_name, "..") == 0) {
					continue;
				}

				ucs2cpy(tmp, path);
				ucs2cat(tmp, filename);

				if (ucs2stat_is_dir(tmp)){
					filepathutil_addslash(tmp);
					find_file(tmp, recursive, callback, instance);
				}
			}
			closedir(dirp);
		}
	}

	return 0;
}

static ucs2_char_t* search_extension(ucs2_char_t* path)
{
	ucs2_char_t* p = ucs2rchr(path, PATHCHAR);
	ucs2_char_t* q = ucs2rchr(path, '.');
	if (q) {
		if (p < q) {
			return q;
		}
	}
	return NULL;
}

ucs2_char_t* filepathutil_addslash(ucs2_char_t* path)
{
	size_t length = ucs2len(path);
	if (length > 0) {
		if (path[length-1] != PATHCHAR) {
			/* Append PATHCHAR to path. */
			path[length++] = PATHCHAR;
			path[length] = 0;
		}
	}
	return (path + length);
}

ucs2_char_t* filepathutil_removeslash(ucs2_char_t* path)
{
	size_t length = ucs2len(path)-1;
	while (length >= 0 && path[length] == PATHCHAR) {
		path[length] = 0;
		length--;
	}
	return (path + length);
}

int filepathutil_hasext(const ucs2_char_t* filename, const ucs2_char_t* ext)
{
	const ucs2_char_t* p = search_extension((ucs2_char_t*)filename);
	if (!p) {
		return 0;
	}
	while (*p && *ext) {
		if (ucs2lower(*p) != ucs2lower(*ext)) {
			return 0;
		}
		p++;
		ext++;
	}
	return (ucs2lower(*p) == ucs2lower(*ext));
}


const ucs2_char_t* filepathutil_combinepath(ucs2_char_t* dst, size_t size, const ucs2_char_t* path, const ucs2_char_t* file)
{
	ucs2cpy(dst, path);
	filepathutil_addslash(dst);
	ucs2cat(dst, file);
	return dst;
}

const ucs2_char_t* filepathutil_skiproot(const ucs2_char_t* path, const ucs2_char_t* root)
{
	if (ucs2ncmp(path, root, ucs2len(root)) == 0) {
		return path + ucs2len(root);
	} else {
		return path;
	}
}

const ucs2_char_t* filepathutil_skippath(const ucs2_char_t* path)
{
	ucs2_char_t* p = ucs2rchr(path, PATHCHAR);
	return p ? p+1 : path;
}

const ucs2_char_t* filepathutil_skip_one_directory(const ucs2_char_t* path)
{
	ucs2_char_t* p = ucs2chr(path, PATHCHAR);
	return p ? p+1 : NULL;
}

void filepathutil_strippath(ucs2_char_t* path)
{
	ucs2_char_t* p = path + ucs2len(path) - 1;
	if (path <= p) {
		if (*p == PATHCHAR) {
			p--;
		}
		while (path <= p) {
			if (*p == PATHCHAR) {
				p++;
				break;
			}
			p--;
		}
		if (path < p) {
			for (;;) {
				*path = *p;
				if (!*p) {
					break;
				}
				path++;
				p++;
			}
		}
	}
}

void filepathutil_replace_slash(ucs2_char_t* path, ucs2_char_t c)
{
	while (*path) {
		if (*path == PATHCHAR) {
			*path = c;
		}
		path++;
	}
}

void filepathutil_add_extension(ucs2_char_t* path, const ucs2_char_t* ext)
{
	ucs2_char_t* p = search_extension(path);
	if (!p) {
		ucs2cat(path, ext);
	}
}

void filepathutil_remove_extension(ucs2_char_t* path)
{
	ucs2_char_t* p = search_extension(path);
	if (p) {
		*p = 0;
	}
}

void filepathutil_remove_filespec(ucs2_char_t* path)
{
	ucs2_char_t* p = ucs2rchr(path, PATHCHAR);
	if (p) {
		*p = 0;
	}
}

int filepathutil_is_relative(const ucs2_char_t* path)
{
	return (*path != PATHCHAR);
}

int filepathutil_file_exists(const ucs2_char_t* path)
{
	return ucs2stat_is_exist(path);
}

int filepathutil_relative_to_absolute(ucs2_char_t* absolute, const ucs2_char_t* base, const ucs2_char_t* relative)
{
	return (rel2abs(relative, base, absolute, MAX_PATH) != NULL);
}

int filepathutil_is_same_root(const ucs2_char_t* path, const ucs2_char_t* root)
{
	return (ucs2ncmp(path, root, ucs2len(root)) == 0);
}

int filepathutil_compare_lastupdate(const ucs2_char_t* file1, const ucs2_char_t* file2)
{
	int ret = 0;
	struct stat st1;
	struct stat st2;

	time_t ret1 = ucs2stat_mtime(file1);
	time_t ret2 = ucs2stat_mtime(file2);
	return (ret1 - ret2);
}

int filepathutil_copyfile(const ucs2_char_t* src, const ucs2_char_t* dst)
{
	int ret = 0;
	FILE *fpi = ucs2fopen(src, "rb");
	FILE *fpo = ucs2fopen(dst, "wb");

	if (!fpi || !fpo) {
		return -1;
	}

	for (;;) {
		char buffer[4096];
		int bytes_read = fread(buffer, sizeof(char), 4096, fpi);
		int bytes_write = fwrite(buffer, sizeof(char), bytes_read, fpo);
		if (bytes_write < bytes_read) {
			ret = -1;
			break;
		}
		if (feof(fpi)) {
			ret = 0;
			break;
		}
	}

	fclose(fpo);
	fclose(fpi);
	return ret;
}

int filepathutil_encode(ucs2_char_t* path)
{
	while (*path) {
		if (*path == 0x002F) {
			*path = 0x005C;
		}
		path++;
	}
	return 0;
}

int filepathutil_decode(ucs2_char_t* path)
{
	while (*path) {
		if (*path == 0x005C) {
			*path = 0x002F;
		}
		path++;
	}
	return 0;
}
