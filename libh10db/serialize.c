/*
 *      Data serializer (with byte-order consideration).
 *
 *      Copyright (c) 2005 Nyaochi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA, or visit
 * http://www.gnu.org/copyleft/gpl.html .
 *
 */

/* $Id: serialize.c,v 1.8 2005/06/04 00:13:05 nyaochi Exp $ */

/*
 * This module reads/writes machine-dependent values from/to:
 *	- 16 bits unsigned integer (little endian)
 *	- 32 bits unsigned integer (little endian)
 *	- UCS-2 string (little endian)
 */

#ifdef	HAVE_CONFIG_H
#include <config.h>
#endif/*HAVE_CONFIG_H*/

#include <os.h>
#include <stdio.h>
#include <stdlib.h>
#include <ucs2char.h>
#include "bufferedfile.h"
#include "serialize.h"


int serialize_uint16(struct bfile *bfp, uint16_t* val, int is_storing)
{
	int ret;
	uint8_t v[2];

	if (is_storing) {
		v[0] = (uint8_t)(*val & 0xFF);
		v[1] = (uint8_t)(*val >> 8);
		ret = (bf_write(bfp, v, sizeof(v)) == sizeof(v) ? 0 : 1);
	} else {
		ret = (bf_read(bfp, v, sizeof(v)) == sizeof(v) ? 0 : 1);
		*val = (uint16_t)v[1] << 8 | (uint16_t)v[0];
	}
	return ret;
}

int serialize_uint32(struct bfile *bfp, uint32_t* val, int is_storing)
{
	int ret;
	uint8_t v[4];

	if (is_storing) {
		v[0] = (uint8_t)(*val & 0xFF);
		v[1] = (uint8_t)(*val >> 8);
		v[2] = (uint8_t)(*val >> 16);
		v[3] = (uint8_t)(*val >> 24);
		ret = (bf_write(bfp, v, sizeof(v)) == sizeof(v) ? 0 : 1);
	} else {
		ret = (bf_read(bfp, v, sizeof(v)) == sizeof(v) ? 0 : 1);
		*val = (uint32_t)v[3] << 24 | (uint32_t)v[2] << 16 | (uint32_t)v[1] << 8 | (uint32_t)v[0];
	}
	return ret;
}

int serialize_ucs2le_char(struct bfile *bfp, ucs2_char_t* val, int is_storing)
{
	int ret;
	uint8_t v[2];

	if (is_storing) {
		v[0] = (uint8_t)(*val & 0xFF);
		v[1] = (uint8_t)(*val >> 8);
		ret = (bf_write(bfp, v, sizeof(v)) == sizeof(v) ? 0 : 1);
	} else {
		ret = (bf_read(bfp, v, sizeof(v)) == sizeof(v) ? 0 : 1);
		*val = (ucs2_char_t)v[1] << 8 | (ucs2_char_t)v[0];
	}
	return ret;
}

static int write_ucs2le_null(struct bfile *bfp)
{
	ucs2_char_t null = 0;
	return serialize_ucs2le_char(bfp, &null, 1);
}

int serialize_ucs2le(struct bfile *bfp, ucs2_char_t** val, int is_storing)
{
	int ret = 0;
	if (is_storing) {
		if (*val) {
			const ucs2_char_t* str = *val;
			size_t i;
			for (i = 0;i < ucs2len(*val)+1;i++) {
				ret |= serialize_ucs2le_char(bfp, (ucs2_char_t*)&str[i], is_storing);
			}
			return ret;
		} else {
			return write_ucs2le_null(bfp);
		}
	} else {
		size_t length = 0;
		ucs2_char_t c;
		*val = 0;

		for (;;) {
			if (serialize_ucs2le_char(bfp, &c, is_storing) != 0) {
				return 1;		/* Read error or file terminates without null character. */
			}
			*val = ucs2realloc(*val, sizeof(ucs2_char_t) * (length+1));
			if (!*val) {
				return 1;		/* Out of memory. */
			}
			(*val)[length++] = c;
			if (!c) {
				return 0;		/* Exit with success. */
			}
		}
	}
}

int serialize_ucs2le_fixed(struct bfile *bfp, ucs2_char_t* val, size_t length, int is_storing)
{
	size_t i;
	int ret = 0;
	for (i = 0;i < length;i++) {
		ret |= serialize_ucs2le_char(bfp, &val[i], is_storing);
	}
	return ret;
}

int serialize_ucs2le_limited(struct bfile *bfp, ucs2_char_t** val, size_t length, int is_storing)
{
	if (is_storing) {
		if (!*val) {
			return write_ucs2le_null(bfp);
		} else if (ucs2len(*val) < length) {
			return serialize_ucs2le_fixed(bfp, *val, ucs2len(*val)+1, is_storing);
		} else {
			ucs2_char_t null = 0;
			int ret = serialize_ucs2le_fixed(bfp, *val, length-1, is_storing);
			ret |= write_ucs2le_null(bfp);
			return ret;
		}
	} else {
		*val = ucs2malloc(sizeof(ucs2_char_t) * length);
		if (*val) {
			return serialize_ucs2le_fixed(bfp, *val, length, is_storing);
		} else {
			return 1;
		}
	}
}
