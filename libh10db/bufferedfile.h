/*
 *      Memory-buffered file I/O routine.
 *
 *      Copyright (c) 2005 Nyaochi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA, or visit
 * http://www.gnu.org/copyleft/gpl.html .
 *
 */

/* $Id: bufferedfile.h,v 1.5 2005/06/03 09:29:56 nyaochi Exp $ */

#ifndef	__BUFFERED_FILE_H__
#define	__BUFFERED_FILE_H__

/**
 * State flag.
 */
enum {
	BF_NONE		= 0x0000,	/**< Normal state. */
	BF_EOF		= 0x0001,	/**< bf_read hits EOF. */
	BF_ERROR	= 0x0002,	/**< Error state. */
};

/**
 * Typedef for a callback function that receives the progress report of reading/writing.
 *	@param	instance			The instance value.
 *	@param	offset				The current offset that is about to read.
 *	@param	size				The whole size of the file.
 *	@return						0 indicates the continue, other value cancel.
 */
typedef int (*bufferedfile_callback)(void *instance, size_t offset, size_t size);

/**
 * Buffered file I/O structure.
 */
struct bfile {
	void *fp;				/**< file pointer. */
	char *buffer;			/**< The pointer to a buffer storing the whole image of a file. */
	size_t buffer_size;		/**< The size of the buffer. */
	size_t buffer_pos;		/**< The current position to be read/written in the buffer */
	size_t block_size;		/**< The size unit of reading/writing a file. */
	int is_storing;			/**< 1 for writing mode, 0 for reading mode. */
	int state;				/**< state flag */
	void *instance;			/**< The instance variable that identifies the caller. */
	bufferedfile_callback progress;		/**< The pointer to a callback function. */
};

/**
 * Open a file (and read the whole content from the file for a reading mode).
 *	@param	filename			A null-terminated string representing a filename.
 *	@param	is_storing			1 indicates writing, 0 does reading.
 *	@param	instance			The instance value that identifies the caller.
 *	@param	pfn					The pointer to a callback function that receives the progress.
 *	@return						The pointer to an instance of struct bfile structure if succeeded,
 *								otherwise NULL.
 */
struct bfile* bf_open(const ucs2_char_t *filename, int is_storing, void *instance, bufferedfile_callback pfn);

/**
 * Close the file (and write the whole content to the file for writing mode).
 *	@param	bfp					The pointer to the struct bfile structure.
 *	@return						0 if succeeded.
 */
int bf_close(struct bfile* bfp);

/**
 * Read the data by the specified amount of size.
 *	@param	bfp					The pointer to the struct bfile structure.
 *	@param	buffer				The pointer to the buffer that receives the data.
 *	@param	size				The size in bytes to be read.
 *	@return						The actual read size.
 */
size_t bf_read(struct bfile* bfp, void *buffer, size_t size);

/**
 * Write the data by the specified amount of size.
 *	@param	bfp					The pointer to the struct bfile structure.
 *	@param	buffer				The pointer to the buffer that holds the data.
 *	@param	size				The size in bytes to be written.
 *	@return						The actual written size.
 */
size_t bf_write(struct bfile* bfp, const void *buffer, size_t size);

/**
 * Get the current offset position.
 *	@param	bfp					The pointer to the struct bfile structure.
 *	@return						The offset position in bytes.
 */
long bf_tell(struct bfile* bfp);

int bf_seek(struct bfile* bfp, long offset);

/**
 * Check whether if the stream reached EOF in the previous read.
 *	@param						1 indicates EOF.
 */
int bf_eof(struct bfile* bfp);

#endif/*__BUFFERED_FILE_H__*/
