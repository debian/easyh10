/*
 *      Memory-buffered file I/O routine.
 *
 *      Copyright (c) 2005 Nyaochi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA, or visit
 * http://www.gnu.org/copyleft/gpl.html .
 *
 */

/* $Id: bufferedfile.c,v 1.11 2005/07/21 20:45:14 nyaochi Exp $ */

/*
 *	We don't need to write this kind of routines any more for recent HDD
 *	with huge amount of read/write cache. But the default H10 HDD drive
 *	mounted on Windows environment doesn't use write cache and has very
 *	slow write access. This routine improves read/write speed by
 *	reading/writing at one time from the H10 HDD. Besides, this module
 *	has a subliminal effect: this module also prevents from crash
 *	during the database writing. If libh10db crashes during the
 *	database writing, the user will get an invalid database that H10
 *	cannot recognize and work properly with.
 */

#ifdef	HAVE_CONFIG_H
#include <config.h>
#endif/*HAVE_CONFIG_H*/

#include <os.h>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>
#include <ucs2char.h>
#include "bufferedfile.h"

#define	BFILE_BLOCK_SIZE	4096

struct bfile* bf_open(const ucs2_char_t *filename, int is_storing, void *instance, bufferedfile_callback pfn)
{
	/* Create struct bfile instance. */
	struct bfile* bfp = (struct bfile*)malloc(sizeof(struct bfile));

	if (bfp) {
		/* Initialize struct bfile structure. */
		memset(bfp, 0, sizeof(*bfp));
		bfp->fp = NULL;
		bfp->instance = instance;
		bfp->progress = pfn;
		bfp->block_size = BFILE_BLOCK_SIZE;
		bfp->is_storing = is_storing;
		bfp->state = 0;

		if (!bfp->is_storing) {
			long filesize = 0;

			/* Open the target file for reading. */
			bfp->fp = ucs2fopen(filename, "rb");
			if (!bfp->fp) {
				goto bf_open_error;
			}

			/* Obtain the file size. */
			if (fseek(bfp->fp, 0, SEEK_END) != 0) {
				goto bf_open_error;
			}
			filesize = ftell(bfp->fp);
			if (filesize == -1) {
				goto bf_open_error;
			}
			bfp->buffer_size = (size_t)filesize;
			if (fseek(bfp->fp, 0, SEEK_SET) != 0) {
				goto bf_open_error;
			}

			/* Allocate buffer that can store the whole file. */
			bfp->buffer = (char*)malloc(bfp->buffer_size);
			if (!bfp->buffer) {
				goto bf_open_error;
			} else {
				char *p = bfp->buffer;
				size_t remaining = bfp->buffer_size;

				/* Read from the file until EOF. */
				for (;;) {
					/* Determine the size of reading. */
					size_t bytes_read, bytes_to_read = remaining;
					if (bfp->block_size < remaining) {
						bytes_to_read = bfp->block_size;
					}

					/* Report the progress. */
					if (bfp->progress) {
						int cont = bfp->progress(
							bfp->instance,
							bfp->buffer_size - remaining,
							bfp->buffer_size
							);
						if (cont != 0) {
							/* Cancel by the caller. */
							goto bf_open_error;
						}
					}

					/* Read from the file. */
					bytes_read = fread(p, sizeof(char), bytes_to_read, bfp->fp);
					if (bytes_read == 0) {
						break;
					}
					p += bytes_read;
					remaining -= bytes_read;
				}

				if (remaining != 0) {
					/* We could not read all contents in the file. */
					goto bf_open_error;
				}
			}
			
			/* Report the termination. */
			if (bfp->progress) {
				bfp->progress(bfp->instance, bfp->buffer_size, bfp->buffer_size);
			}

			/* Close the file. */
			fclose(bfp->fp);
			bfp->fp = NULL;

		} else {
			/* Open the target file for writing. */
			FILE *fp = ucs2fopen(filename, "wb");
			if (!fp) {
				goto bf_open_error;
			}

			/* Store the *fp for bf_close(). */
			bfp->fp = fp;
		}
	}
	return bfp;

bf_open_error:
	bf_close(bfp);
	return NULL;
}

int bf_close(struct bfile* bfp)
{
	int ret = 0;

	if (bfp->is_storing && bfp->fp) {
		/* The file must be opened by bf_open(). */
		FILE *fp = (FILE*)bfp->fp;
		char *p = bfp->buffer;
		size_t remaining = bfp->buffer_pos;

		while (remaining > 0) {
			/* Determine the size of writing. */
			size_t written_bytes = 0, bytes_to_write = remaining;
			if (bytes_to_write > bfp->block_size) {
				bytes_to_write = bfp->block_size;
			}

			/* Report the progress. */
			if (bfp->progress) {
				int cont = bfp->progress(
					bfp->instance,
					bfp->buffer_pos - remaining,
					bfp->buffer_pos
					);
				if (cont != 0) {
					/* Cancel by the caller. */
					ret = 1;
					break;
				}
			}

			/* Write. */
			written_bytes = fwrite(p, sizeof(char), bytes_to_write, fp);
			if (written_bytes == 0) {
				ret = 1;
				break;
			}

			p += written_bytes;
			remaining -= written_bytes;
		}
	}

	/* Report the termination. */
	if (bfp->progress) {
		bfp->progress(bfp->instance, bfp->buffer_size, bfp->buffer_size);
	}

	if (bfp->fp) {
		/* Close the file. */
		fclose(bfp->fp);
		bfp->fp = NULL;
	}

	/* Release the memory allocated by bf_open(). */
	free(bfp->buffer);
	free(bfp);

	return ret;
}

size_t bf_read(struct bfile* bfp, void *buffer, size_t size)
{
	size_t remaining = bfp->buffer_size - bfp->buffer_pos;
	if (remaining <= 0) {
		bfp->state = BF_EOF;
		return 0;
	}
	if (remaining < size)	size = remaining;
	memcpy(buffer, bfp->buffer + bfp->buffer_pos, size);
	bfp->buffer_pos += size;
	return size;
}

size_t bf_write(struct bfile* bfp, const void *buffer, size_t size)
{
	size_t end = bfp->buffer_pos + size;
	while (bfp->buffer_size < end) {
		bfp->buffer = (char *)realloc(bfp->buffer, bfp->buffer_size + bfp->block_size);
		if (!bfp->buffer) {
			bfp->state = BF_ERROR;
			return 0;
		}
		bfp->buffer_size += bfp->block_size;
	}
	memcpy(bfp->buffer + bfp->buffer_pos, buffer, size);
	bfp->buffer_pos += size;
	return size;
}

long bf_tell(struct bfile* bfp)
{
	return (long)bfp->buffer_pos;
}

int bf_seek(struct bfile* bfp, long offset)
{
	if (offset < bfp->buffer_size) {
		bfp->buffer_pos = offset;
		return 0;
	} else {
		return 1;
	}
}

int bf_eof(struct bfile* bfp)
{
	return (bfp->state & BF_EOF);
}
