#!/bin/sh
# $Id: autogen.sh,v 1.4 2006/01/21 19:23:35 nyaochi Exp $

if [ "$1" = "--force" ];
then
    FORCE=--force
    NOFORCE=
    FORCE_MISSING=--force-missing
else
    FORCE=
    NOFORCE=--no-force
    FORCE_MISSING=
fi

aclocal || {
    echo "aclocal failed!"
    exit 1
}

libtoolize --copy $FORCE 2>&1 | sed '/^You should/d' || {
    echo "libtoolize failed!"
    exit 1
}

autoheader $FORCE || {
    echo "autoheader failed!"
    exit 1
}

automake -a -c $NOFORCE $FORCE_MISSING || {
    echo "automake failed!"
    exit 1
}

autoconf $FORCE || {
    echo "autoconf failed!"
    exit 1
}
