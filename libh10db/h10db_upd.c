/*
 *      H10DB.upd: Detector of music file update.
 *
 *      Copyright (c) 2005 Nyaochi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA, or visit
 * http://www.gnu.org/copyleft/gpl.html .
 *
 */

/* $Id: h10db_upd.c,v 1.11 2005/06/02 21:34:39 nyaochi Exp $ */

/*
 * We don't take machine dependent factors such as word-order and
 * sizeof(time_t) into account because this module just suggests us
 * to rebuild the database from the incompatible machine.
 */

#ifdef	HAVE_CONFIG_H
#include <config.h>
#endif/*HAVE_CONFIG_H*/

#include <os.h>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <ucs2char.h>
#include "bufferedfile.h"
#include "crc.h"

#include <h10db.h>

#define	UPD_MAGIC		(1234567890 + 2)

int h10db_upd_read(h10db_upd_t* upd, struct bfile* bfp)
{
	upd->num = 0;
	upd->array = NULL;

	/* Open H10DB.upd. */
	if (bfp) {
		int ret = 0;
		do {
			uint32_t magic;

			/*
			 * We don't have to take endian into account.
			 */

			/* Check the file magic. */
			if (bf_read(bfp, &magic, sizeof(magic)) != sizeof(magic)) {
				ret = 1;
				break;
			}
			if (magic != UPD_MAGIC) {
				ret = 1;
				break;
			}

			/* Check the size of time_t. */
			if (bf_read(bfp, &magic, sizeof(magic)) != sizeof(magic)) {
				ret = 1;
				break;
			}
			if ((size_t)magic != sizeof(time_t)) {
				ret = 1;
				break;
			}

			/* Read the number of elements. */
			if (bf_read(bfp, &upd->num, sizeof(upd->num)) != sizeof(upd->num)) {
				ret = 1;
				break;
			}

			/* Allocate array. */
			upd->array = (h10db_upd_entry_t*)malloc(sizeof(h10db_upd_entry_t) * upd->num);
			if (!upd->array) {
				ret = 1;
				break;
			}

			/* Read the contents. */
			if (bf_read(bfp, upd->array, sizeof(h10db_upd_entry_t) * upd->num) != sizeof(h10db_upd_entry_t) * upd->num) {
				free(upd->array);
				ret = 1;
				break;
			}
		} while (0);

		bf_close(bfp);
		return ret;
	} else {
		/* If H10DB.upd file does not exist, does nothing. */
		return 1;
	}
}

int h10db_upd_is_updated(h10db_upd_t* upd, uint32_t entry, h10db_dat_t* item, const ucs2_char_t *filename)
{
	uint32_t crc;

	/* If upd is not initialized, force update. */
	if (!upd) {
		return 1;
	}

	crc  = crcSlow((const char *)item->file_path, (int)(ucs2len(item->file_path) * sizeof(ucs2_char_t)));
	crc += crcSlow((const char *)item->file_name, (int)(ucs2len(item->file_name) * sizeof(ucs2_char_t)));

	if (upd->num <= entry) {
		upd->array = (h10db_upd_entry_t*)realloc(upd->array, sizeof(h10db_upd_entry_t) * (entry+1));
		if (upd->array) {
			memset(&upd->array[upd->num], 0, sizeof(h10db_upd_entry_t) * (entry+1-upd->num));
			upd->num = entry+1;

			upd->array[entry].crc  = crc;
			upd->array[entry].ft = ucs2stat_mtime(filename);
		}
		return 1;		/* New entry needs update. */
	} else {
		time_t mtime = ucs2stat_mtime(filename);
		if (upd->array[entry].crc != crc || upd->array[entry].ft != mtime) {
			upd->array[entry].crc = crc;
			upd->array[entry].ft = mtime;
			return 1;	/* Updated entry needs update. */
		} else {
			return 0;	/* Non-updated entry. */
		}
	}
}

int h10db_upd_write(const h10db_upd_t* upd, struct bfile* bfp)
{
	if (upd) {
		const uint32_t magic = UPD_MAGIC;
		const uint32_t time_size = sizeof(time_t); 

		if (bf_write(bfp, &magic, sizeof(magic)) != sizeof(magic)) {
			goto h10db_upd_write_error;
		}
		if (bf_write(bfp, &time_size, sizeof(time_size)) != sizeof(time_size)) {
			goto h10db_upd_write_error;
		}
		if (bf_write(bfp, &upd->num, sizeof(upd->num)) != sizeof(upd->num)) {
			goto h10db_upd_write_error;
		}
		if (bf_write(bfp, upd->array, sizeof(h10db_upd_entry_t) * upd->num) != sizeof(h10db_upd_entry_t)*upd->num) {
			goto h10db_upd_write_error;
		}
		bf_close(bfp);
	}
	return 0;

h10db_upd_write_error:
	bf_close(bfp);
	return H10DBE_UPD_WRITE;
}
