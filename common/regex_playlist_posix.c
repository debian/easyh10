/*
 *      Regular expression playlist (with POSIX/Rx regex).
 *
 *      Copyright (c) 2005-2006 Nyaochi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA, or visit
 * http://www.gnu.org/copyleft/gpl.html .
 *
 */

/* $Id: regex_playlist_posix.c,v 1.1 2006/02/07 00:42:04 nyaochi Exp $ */

#ifdef	HAVE_CONFIG_H
#include <config.h>
#endif/*HAVE_CONFIG_H*/

#include <os.h>
#include <ctype.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#if defined(HAVE_POSIX_REGEX)
#include <regex.h>
#elif defined(HAVE_GNU_RX)
#include <rxposix.h>
#endif

#include <ucs2char.h>

#include "filepathutil.h"

#include "regex_playlist.h"

struct tag_regex_condition {
	regex_t reg;
	ucs2_char_t* str;
	int regex_pattern;
	int value;
};
typedef struct tag_regex_condition regex_condition_t;

struct tag_regex_playlist {
	regex_condition_t* conditions;
	size_t num_conditions;
};

void regex_playlist_init(regex_playlist_t** ptr_rep)
{
	regex_playlist_t* rep = malloc(sizeof(regex_playlist_t));
	memset(rep, 0, sizeof(*rep));
	*ptr_rep = rep;
}

void regex_playlist_finish(regex_playlist_t* rep)
{
	size_t i;
	for (i = 0;i < rep->num_conditions;++i) {
		if (rep->conditions[i].regex_pattern) {
			regfree(&rep->conditions[i].reg);
		} else {
			ucs2free(rep->conditions[i].str);
		}
	}
	free(rep->conditions);
	free(rep);
}

static char *strip(char *line)
{
	char *p = line;
	while (*p && *p != '\n' && *p != '\r') {
		p++;
	}
	*p = 0;

	return line;
}

int regex_playlist_read(regex_playlist_t* rep, const ucs2_char_t* filename)
{
	FILE *fp = ucs2fopen(filename, "r");
	if (!fp) {
		return -1;
	} else {
		unsigned char *p = NULL;
		char line[MAX_PATH * 8];
		int lines = 0;
		int is_utf8 = 0;

		rep->conditions = 0;
		rep->num_conditions = 0;

		while (fgets(line, sizeof(line)-1, fp) != NULL) {
			char* exp = NULL;
			int flag = 0, value = 0;
			strip(line);

			p = (unsigned char *)line;
#if 0	/* No UTF-8 support for now. */
			if (lines == 0) {
				/* Skip UTF-8 BOM if any. */
				if (strlen((char *)p) > 3) {
					if (p[0] == 0xEF && p[1] == 0xBB && p[2] == 0xBF) {
						p += 3;
						is_utf8 = 1;
					}
				}
			}
#endif
			++lines;

			if (p[0] == 0 || p[0] == '#') {
				continue;
			}

			if (strlen(p) < 3) {
				continue;
			}

			switch (p[0]) {
			case '+':
				value = 1;
				break;
			case '-':
				value = 0;
				break;
			default:
				continue;
			}

			switch (p[1]) {
			case 's':
				flag = -1;
				break;
			case 'r':
				flag = REG_EXTENDED|REG_ICASE|REG_NOSUB|REG_NEWLINE;
				break;
			default:
				continue;
			}

			if (p[2] != ' ') {
				continue;
			}

			p += 3;

			exp = p;
			if (exp) {
				regex_condition_t* cond = NULL;
				rep->conditions = (regex_condition_t*)realloc(rep->conditions, sizeof(regex_condition_t) * (rep->num_conditions+1));
				cond = &rep->conditions[rep->num_conditions];
				cond->str = NULL;
				if (flag == -1) {
					cond->regex_pattern = 0;
					cond->str = mbsdupucs2(exp);
					ucs2lwr(cond->str);
				} else {
					cond->regex_pattern = 1;
					if (regcomp(&cond->reg, exp, flag) != 0) {
						return lines;
					}
				}
				cond->value = value;
				rep->num_conditions++;
			}
		}

		fclose(fp);
		return 0;
	}
}

int regex_playlist_test(regex_playlist_t* rep, const ucs2_char_t* filename)
{
	size_t i;
	int value = 0;
	for (i = 0;i < rep->num_conditions;++i) {
		regex_condition_t* cond = &rep->conditions[i];
		if (cond->regex_pattern) {
			char *mbs = ucs2dupmbs(filename);
			if (mbs) {
				if (regexec(&cond->reg, mbs, 0, NULL, 0) == 0) {
					value = cond->value;
				}
				ucs2free(mbs);
			}
		} else {
			ucs2_char_t* ucs2 = ucs2dup(filename);
			if (ucs2) {
				ucs2lwr(ucs2);
				if (ucs2str(ucs2, cond->str)) {
					value = cond->value;
				}
				ucs2free(ucs2);
			}
		}
	}
	return value;
}
