/*
 *      H10DB model definition (*.model) parser/writer.
 *
 *      Copyright (c) 2005 Nyaochi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA, or visit
 * http://www.gnu.org/copyleft/gpl.html .
 *
 */

/* $Id: h10db_model.c,v 1.5 2005/10/20 17:13:09 nyaochi Exp $ */

#ifdef	HAVE_CONFIG_H
#include <config.h>
#endif/*HAVE_CONFIG_H*/

#include <os.h>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <ucs2char.h>
#include "bufferedfile.h"
#include "serialize.h"

#include <h10db.h>

int h10db_model_findchunk(struct bfile *bfp, const char *chunk_name, uint32_t* chunk_size)
{
	while (!bf_eof(bfp)) {
		/* Read a chunk. */
		int ret = 0;
		long pos = 0;
		uint32_t size;
		char magic[4];

		pos = bf_tell(bfp);
		ret |= (bf_read(bfp, magic, 4) == 4) ? 0 : 1;
		ret |= serialize_uint32(bfp, &size, 0);
		if (ret != 0) {
			return bf_eof(bfp) ? H10DBE_MODEL_FINDCHUNK : H10DBE_MODEL_READ;
		}

		/* Compare the chunk name. */
		if (strncmp(magic, chunk_name, 4) == 0) {
			*chunk_size = size;
			return 0;
		} else {
			if (bf_seek(bfp, pos + size) != 0) {
				return H10DBE_MODEL_READ;
			}
		}
	}

	return H10DBE_MODEL_FINDCHUNK;
}

int h10db_model_writechunk(struct bfile *bfp, const char *chunk_name, uint32_t chunk_size, long* offset)
{
	int ret = 0;
	chunk_size += 8;
	ret |= (bf_write(bfp, chunk_name, 4) == 4) ? 0 : 1;
	if (offset) {
		*offset = bf_tell(bfp);
	}
	ret |= serialize_uint32(bfp, &chunk_size, 1);
	return ret;
}

int h10db_model_writechunksize(struct bfile *bfp, long offset, uint32_t chunk_size)
{
	int ret = 0;
	long pos = bf_tell(bfp);
	chunk_size += 8;
	bf_seek(bfp, offset);
	ret |= serialize_uint32(bfp, &chunk_size, 1);
	bf_seek(bfp, pos);
	return ret;
}


uint32_t h10db_model_get_padding(h10db_type_t* type)
{
	uint32_t fw = (type->model & H10DB_FIRMWARE_UNMASK);
	switch (fw) {
	case H10DB_FIRMWARE_UMS:
		return 1032;
	case H10DB_FIRMWARE_MTP:
	case H10DB_FIRMWARE_MTP_2_50:
		return 0;
	default:
		return 0;	/* Should not come here. */
	}
}

uint32_t h10db_model_get_maxentries(h10db_type_t* type)
{
	uint32_t capacity = (type->model & H10DB_CAPACITY_UNMASK);
	switch (capacity) {
	case H10DB_CAPACITY_5GB:
	case H10DB_CAPACITY_6GB:
		return 4000;
	case H10DB_CAPACITY_20GB:
		return 8000;
	default:
		return 4000; /* Should not came here. */
	}
}

uint32_t h10db_model_has_hdr_unknown5(h10db_type_t* type)
{
	uint32_t fw = (type->model & H10DB_FIRMWARE_UNMASK);
	switch (fw) {
	case H10DB_FIRMWARE_MTP_2_50:
		return 1;
	case H10DB_FIRMWARE_MTP:
	case H10DB_FIRMWARE_UMS:
		return 0;
	default:
		return 0;	/* Should not come here. */
	}	
}




void h10db_type_init(h10db_type_t* type, uint32_t h10db_flag)
{
	memset(type, 0, sizeof(*type));
	type->version = 1;
	type->model = (h10db_flag & 0xFFFF);
}

int h10db_type_serialize(struct bfile *bfp, h10db_type_t* type, int is_storing)
{
	int ret = 0;
	ret |= serialize_uint16(bfp, &type->version, is_storing);
	ret |= serialize_uint16(bfp, &type->model, is_storing);
	ret |= serialize_uint16(bfp, &type->fw_major_min, is_storing);
	ret |= serialize_uint16(bfp, &type->fw_minor_min, is_storing);
	ret |= serialize_uint16(bfp, &type->fw_major_max, is_storing);
	ret |= serialize_uint16(bfp, &type->fw_minor_max, is_storing);
	return ret;
}

int h10db_type_read(struct bfile *bfp, h10db_type_t* type)
{
	int ret = 0;
	uint32_t chunk_size = 0;

	if (bf_seek(bfp, 0) != 0) {
		return H10DBE_MODEL_READ;
	}
	ret = h10db_model_findchunk(bfp, "TYPE", &chunk_size);
	if (ret != 0) {
		return ret;
	}
	ret = h10db_type_serialize(bfp, type, 0);
	return ret;
}

int h10db_type_write(struct bfile *bfp, h10db_type_t* type)
{
	int ret = 0;
	uint32_t chunk_size = 0;

	ret = h10db_model_writechunk(bfp, "TYPE", sizeof(*type), NULL);
	if (ret != 0) {
		return ret;
	}
	ret = h10db_type_serialize(bfp, type, 1);
	return ret;
}


uint16_t h10db_get_model(h10db_t* h10db)
{
	return h10db->type->model;
}

uint16_t h10db_get_capacity(h10db_t* h10db)
{
	return (h10db->type->model >> 8);
}

uint16_t h10db_get_type(h10db_t* h10db)
{
	return (h10db->type->model & 0xFF);
}

uint16_t h10db_get_fw_major_min(h10db_t* h10db)
{
	return h10db->type->fw_major_min;
}

uint16_t h10db_get_fw_minor_min(h10db_t* h10db)
{
	return h10db->type->fw_minor_min;
}

uint16_t h10db_get_fw_major_max(h10db_t* h10db)
{
	return h10db->type->fw_major_max;
}

uint16_t h10db_get_fw_minor_max(h10db_t* h10db)
{
	return h10db->type->fw_minor_max;
}
