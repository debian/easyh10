/*
 *      EasyH10 template generation routine.
 *
 *      Copyright (c) 2005 Nyaochi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA, or visit
 * http://www.gnu.org/copyleft/gpl.html .
 *
 */

/* $Id: easyh10_template.c,v 1.13 2005/10/22 23:11:49 nyaochi Exp $ */

#ifdef	HAVE_CONFIG_H
#include <config.h>
#endif/*HAVE_CONFIG_H*/

#include <os.h>
#include <stdio.h>
#include <string.h>
#include <ucs2char.h>

#include <h10db.h>
#include "easyh10.h"

static int dummy_progress_callback(void *instance, int phase, int param_int, void *param_ptr)
{
	return 0;
}

static void dummy_error_callback(void *instance, int code, void *param)
{
}

int easyh10_generate_template(
	const ucs2_char_t* path_to_db,
	const ucs2_char_t* filename,
	int flags,
	uint16_t fw_major_min,
	uint16_t fw_minor_min,
	uint16_t fw_major_max,
	uint16_t fw_minor_max,
	void *instance,
	update_progress_callback progress_proc,
	easyh10_error_callback error_proc
	)
{
	int ret = 0;
	h10db_t *h10db = h10db_new(flags);
	h10db_type_t* type = NULL;
	if (!h10db) {
		error_proc(instance, EASYH10_ERROR_INITLIBH10DB, NULL);
		return -1;
	}

	type = h10db_access_type(h10db);
	type->version = 1;
	type->fw_major_min = fw_major_min;
	type->fw_minor_min = fw_minor_min;
	type->fw_major_max = fw_major_max;
	type->fw_minor_max = fw_minor_max;

	ret = h10db_read(h10db, path_to_db);
	if (ret != 0) {
		error_proc(instance, EASYH10_ERROR_READDB, &ret);
		h10db_delete(h10db);
		return -1;
	}

	ret = h10db_store_model(h10db, filename);
	if (ret != 0) {
		error_proc(instance, EASYH10_ERROR_STORETEMPLATE, &ret);
		h10db_delete(h10db);
		return -1;
	}

	h10db_delete(h10db);

	return 0;
}

static const ucs2_char_t ucs2cs_system[] = {'S','y','s','t','e','m',0};
static const ucs2_char_t ucs2cs_system_data[] = {'S','y','s','t','e','m','\\','D','A','T','A',0};
static const ucs2_char_t ucs2cs_media_music[] = {'M','e','d','i','a','\\','M','u','s','i','c',0};
static const ucs2_char_t ucs2cs_media_playlist[] = {'M','e','d','i','a','\\','P','l','a','y','l','i','s','t',0};
static const ucs2_char_t ucs2cs_music[] = {'M','u','s','i','c',0};
static const ucs2_char_t ucs2cs_playlist[] = {'P','l','a','y','l','i','s','t','s',0};
static const ucs2_char_t ucs2cs_plp[] = {'.','p','l','p',0};
static const ucs2_char_t ucs2cs_pla[] = {'.','p','l','a',0};

int easyh10_get_modelenv(
	easyh10_modelenv_t* env,
	const ucs2_char_t* model_filename,
	void *instance,
	update_progress_callback progress_proc,
	easyh10_error_callback error_proc
	)
{
	int ret = 0;

	h10db_t *h10db = h10db_new(0);
	if (!h10db) {
		error_proc(instance, EASYH10_ERROR_INITLIBH10DB, NULL);
		return 1;
	}

	if (!progress_proc) {
		progress_proc = dummy_progress_callback;
	}
	if (!error_proc) {
		error_proc = dummy_error_callback;
	}

	ret = h10db_load_model(h10db, model_filename);
	if (ret != 0) {
		error_proc(instance, EASYH10_ERROR_LOADTEMPLATE, &ret);
		ret = 1;
		goto modelenv_exit;
	}

	switch (h10db_get_type(h10db)) {
	case H10DB_FIRMWARE_UMS:
	default:
		env->system_location = ucs2cs_system;
		env->database_location = ucs2cs_system_data;
		env->music_location = ucs2cs_media_music;
		env->playlist_location = ucs2cs_media_playlist;
		env->playlist_extension = ucs2cs_plp;
		break;
	case H10DB_FIRMWARE_MTP:
	case H10DB_FIRMWARE_MTP_2_50:
		env->system_location = ucs2cs_system;
		env->database_location = ucs2cs_system_data;
		env->music_location = ucs2cs_music;
		env->playlist_location = ucs2cs_playlist;
		env->playlist_extension = ucs2cs_pla;
		break;
	}

modelenv_exit:
	h10db_delete(h10db);
	return ret;
}
