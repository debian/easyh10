/*
 *      Regular expression playlist.
 *
 *      Copyright (c) 2005-2006 Nyaochi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA, or visit
 * http://www.gnu.org/copyleft/gpl.html .
 *
 */

/* $Id: regex_playlist.h,v 1.1 2006/01/23 01:55:59 nyaochi Exp $ */

#ifndef	__REGEX_PLAYLIST_H__
#define	__REGEX_PLAYLIST_H__

#ifdef	__cplusplus
extern "C" {
#endif/*__cplusplus*/

struct tag_regex_playlist; typedef struct tag_regex_playlist regex_playlist_t;

void regex_playlist_init(regex_playlist_t** rep);
void regex_playlist_finish(regex_playlist_t* rep);
int regex_playlist_read(regex_playlist_t* rep, const ucs2_char_t* filename);
int regex_playlist_test(regex_playlist_t* rep, const ucs2_char_t* filename);

#ifdef	__cplusplus
}
#endif/*__cplusplus*/

#endif/*__REGEX_PLAYLIST_H__*/
