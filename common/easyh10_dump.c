/*
 *      EasyH10 dump routine.
 *
 *      Copyright (c) 2005 Nyaochi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA, or visit
 * http://www.gnu.org/copyleft/gpl.html .
 *
 */

/* $Id: easyh10_dump.c,v 1.12 2005/07/21 17:09:12 nyaochi Exp $ */

#ifdef	HAVE_CONFIG_H
#include <config.h>
#endif/*HAVE_CONFIG_H*/

#include <os.h>
#include <stdio.h>
#include <stdlib.h>
#include <ucs2char.h>
#include <h10db.h>

int easyh10_dump(
	const ucs2_char_t *path_to_db,
	FILE *fp,
	const ucs2_char_t *model_filename,
	int flags
	)
{
	int i, j;
	int ret;
	FILE *fpo = fp;
	h10db_t* h10db = NULL;
	
	/* Create an instance of h10db_t. */
	h10db = h10db_new(flags);
	if (!h10db) {
		return -1;
	}

	if (model_filename && *model_filename) {
		ret = h10db_load_model(h10db, model_filename);
		if (ret != 0) {
			fprintf(stderr, "%d\n", ret);
			h10db_delete(h10db);
			return 1;
		}
	}

	/* Read the database from the path specified. */
	ret = h10db_read(h10db, path_to_db);
	if (ret != 0) {
		fprintf(stderr, "%d\n", ret);
		h10db_delete(h10db);
		return 1;
	}

	/* Dump the H10DB.hdr */
	h10db_hdr_repr(fpo, h10db->hdr);
	fprintf(fpo, "\n");

	/* Dump the H10DB.dat */
	fprintf(fpo, "# H10DB.dat {num_dat = %d}\n", h10db->hdr->num_dat_entries);
	for (i = 0;i < (int)h10db->hdr->num_dat_entries;i++) {
		fprintf(fpo, "## Entry %d\n", i);
		h10db_dat_repr(fpo, &h10db->dat[i]);
		h10db_dat_validate(fpo, &h10db->dat[i]);
		fprintf(fpo, "## End of entry %d\n", i);
	}
	fprintf(fpo, "# End of H10DB.dat\n");
	fprintf(fpo, "\n");

	/* Dump all H10DB_*.idx files. */
	for (i = 0;i < (int)h10db->hdr->num_dat_fields;i++) {
		if (h10db->idx[i]) {
			h10db_idx_t* idx = h10db->idx[i];
			char *pathname = ucs2dupmbs(h10db->hdr->fd[i].index_pathname);

			/* Start of the dump. */
			fprintf(
				fpo,
				"# %s {index = %d; id = 0x%08X} \n",
				pathname, i, h10db->hdr->fd[i].id
				);

			/* Dump all the elements in the index. */
			for (j = 0;j < (int)h10db->hdr->num_dat_entries;j++) {
				uint32_t check_value = 0;
				h10db_dat_t* dat_entry = &h10db->dat[idx[j].index];
				ucs2_char_t* ucs2_title = dat_entry->title;
				char *file_path = ucs2dupmbs(dat_entry->file_path);
				char *file_name = ucs2dupmbs(dat_entry->file_name);
				char *title = NULL;
				char *artist = ucs2dupmbs(dat_entry->artist);
				char *album = ucs2dupmbs(dat_entry->album);

				if (is_ucs2surrogate(*ucs2_title)) {
					if (is_ucs2surrogate(*++ucs2_title)) {
						ucs2_title++;
					}
				}
				title = ucs2dupmbs(ucs2_title);

				/* Dump this element. */
				h10db_idx_repr(fpo, &idx[j]);

				/* Show the information of the entry pointed by the index. */
				fprintf(fpo, "(%s - %s / %s [%s%s])\n",	title, artist, album, file_path, file_name);

				/* Calculate the check value from the actual field. */
				check_value = h10db_dat_calculate_checkvalue(dat_entry, i);
				/* Show warning if the calculated value does not equal to the check_value. */
				if (idx[j].check_value != check_value) {
					fprintf(fpo, "WARNING: CRC mismatch {calculated value = 0x%08X}.\n", check_value);
				}
				fprintf(fpo, "\n");

				ucs2free(title);
				ucs2free(album);
				ucs2free(artist);
				ucs2free(file_name);
				ucs2free(file_path);
			}

			/* End of the dump. */
			fprintf(fpo, "# End of %s\n", pathname);

			ucs2free(pathname);
		} else {
			fprintf(fpo, "# Skiping field %d {id = 0x%08X}\n", i, h10db->hdr->fd[i].id);
		}
	}

	/* Delete the h10db_t instance. */
	h10db_delete(h10db);

	/* Exit with success. */
	return 0;
}
