/*
 *      EasyH10 playlist conversion routine.
 *
 *      Copyright (c) 2005 Nyaochi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA, or visit
 * http://www.gnu.org/copyleft/gpl.html .
 *
 */

/* $Id: easyh10_playlist.c,v 1.27 2006/02/07 01:45:40 nyaochi Exp $ */

#ifdef	HAVE_CONFIG_H
#include <config.h>
#endif/*HAVE_CONFIG_H*/

#include <os.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ucs2char.h>

#include "easyh10.h"
#include <playlist.h>
#include <regex_playlist.h>
#include <filepathutil.h>

struct tag_playlist_target_file {
	ucs2_char_t path[MAX_PATH];
	ucs2_char_t file[MAX_PATH];
	int playlist_in_music;
	int update;
};
typedef struct tag_playlist_target_file playlist_target_file_t;

struct tag_findfile_playlist_dat {
	playlist_target_file_t* playlists;
	int num_playlists;
	int playlist_in_music;
	int regex_playlist;
	void *instance;
	update_progress_callback proc;
};
typedef struct tag_findfile_playlist_dat findfile_playlist_dat_t;

struct tag_media_target_file {
	ucs2_char_t path[MAX_PATH];
	ucs2_char_t file[MAX_PATH];
};
typedef struct tag_media_target_file media_target_file_t;

struct tag_findfile_music {
	media_target_file_t* targets;
	int num_targets;
	void *instance;
	update_progress_callback proc;
};
typedef struct tag_findfile_music findfile_music_t;

static const ucs2_char_t ucs2cs_m3u[] = {'.','m','3','u',0};
static const ucs2_char_t ucs2cs_m3u8[] = {'.','m','3','u','8',0};
static const ucs2_char_t ucs2cs_pls[] = {'.','p','l','s',0};
static const ucs2_char_t ucs2cs_rep[] = {'.','r','e','p',0};


#ifdef	_WIN32
#define COMP_STR(x, y)	lstrcmpiW(x, y)	/* FAT32 treats upper/lower letters as identical. */
#else
#define COMP_STR(x, y)	ucs2cmp(x, y)
#endif

static int comp_filename(const void *_x, const void *_y)
{
	media_target_file_t* x = (media_target_file_t*)_x;
	media_target_file_t* y = (media_target_file_t*)_y;
	return COMP_STR(x->file, y->file);
}

static int comp_pathfile(const void *_x, const void *_y)
{
	media_target_file_t* x = (media_target_file_t*)_x;
	media_target_file_t* y = (media_target_file_t*)_y;
	int ret = COMP_STR(x->path, y->path);
	if (ret == 0) {
		return COMP_STR(x->file, y->file);
	} else {
		return ret;
	}
}

static int find_musicfile(findfile_music_t* ffd_music, const ucs2_char_t* filename, int *begin, int *end)
{
	int low = 0, high = ffd_music->num_targets-1;

	/* Binary search. */
	while (low <= high) {
		int middle = (low + high) / 2;
		int comp = COMP_STR(filename, ffd_music->targets[middle].file);
		if (comp == 0) {
			/* Found */
			*begin = *end = middle;
			while (*begin >= 0 && COMP_STR(filename, ffd_music->targets[*begin].file) == 0) {
				(*begin)--;
			}
			(*begin)++;
			while (*end < ffd_music->num_targets && COMP_STR(filename, ffd_music->targets[*end].file) == 0) {
				(*end)++;
			}
			return 1;
		} else if (comp < 0) {
			high = middle - 1;
		} else {
			low = middle + 1;
		}
	}
	return 0;
}



static int guess_missing_music(
	ucs2_char_t* music_file,
	const ucs2_char_t* path_to_music
	)
{
	/*
		A very simple guessing approach. Given a missing music file and path_to_music,
			music_file:		C:\home\nyaochi\music\artist\album\01_track.mp3
			path_to_music:	D:\System\Music
		this function search for the following paths in this order (priority):
			D:\System\Music\home\nyaochi\music\artist\album\01_track.mp3
			D:\System\Music\nyaochi\music\artist\album\01_track.mp3
			D:\System\Music\music\artist\album\01_track.mp3
			D:\System\Music\artist\album\01_track.mp3
			D:\System\Music\album\01_track.mp3
			D:\System\Music\01_track.mp3
		hoping that we can find one of these files located in the player.
	*/
	ucs2_char_t *p = music_file;

	if (!music_file) {
		return 1;
	}

	while (p = (ucs2_char_t*)filepathutil_skip_one_directory(p)) {
		ucs2_char_t candidate[MAX_PATH*2];

		/* Generate a candidate. */
		ucs2cpy(candidate, path_to_music);
		filepathutil_addslash(candidate);
		ucs2cat(candidate, p);

		/* Limit the length of candidate to avoid buffer overflow. */
		if (ucs2len(candidate) >= MAX_PATH) {
			continue;
		}

		/* Find the file. */
		if (filepathutil_file_exists(candidate)) {
			ucs2cpy(music_file, candidate);
			return 0;
		}
	}

	return 1;
}

static int count_frequency(const ucs2_char_t* str, const ucs2_char_t* substr)
{
	const ucs2_char_t *p = str;
	int freq = 0;
	while (p = ucs2str(p, substr)) {
		p++;
		freq++;
	}
	return freq;
}

static double calc_similarity(const ucs2_char_t* x, const ucs2_char_t* y)
{
	int match = 0;
	int nx = ucs2len(x);
	int ny = ucs2len(y);

	/* Calculate Jaccard coefficients of letter bi-grams. */
	while (*x) {
		int freq = 0;
		ucs2_char_t bigram_x[3];
		bigram_x[0] = *x;
		bigram_x[1] = *(x+1);
		bigram_x[2] = 0;

		freq = count_frequency(y, bigram_x);
		if (freq > 0) {
			match++;
		}
		x++;
	}

	if (nx == 0 || ny == 0) {
		return 0;
	} else {
		return (double)match / (double)(nx + ny - match);
	}
}

static int search_missing_music(
	ucs2_char_t* music_file,
	const ucs2_char_t* path_to_music,
	findfile_music_t* ffd_music
	)
{
	int begin = 0, end = 0;
	const ucs2_char_t* filename = NULL;
	ucs2_char_t pathname[MAX_PATH];

	if (!music_file) {
		return 1;
	}

	/* Obtain filename and pathname. */
	filename = filepathutil_skippath(music_file);
	ucs2cpy(pathname, music_file);
	filepathutil_remove_filespec(pathname);
	
	/* Search for the file name in music folders. */
	if (find_musicfile(ffd_music, filename, &begin, &end)) {
		/* Found music file(s) that have the filename. */
		media_target_file_t* found_music = &ffd_music->targets[begin];

		if (begin + 1 != end) {
			int i, j;
			double max_similarity = 0.0;

			/* Calculate similarities of paths using Jaccard coefficient. */
			for (i = begin;i < end;i++) {
				double similarity = calc_similarity(ffd_music->targets[i].path, pathname);
				if (max_similarity < similarity) {
					j = i;
					max_similarity = similarity;
				}
			}
			found_music = &ffd_music->targets[j];
		}

		ucs2cpy(music_file, found_music->path);
		ucs2cat(music_file, found_music->file);
		return 0;
	}

	return 1;
}



int normalize_playlist(
	playlist_t* pl,
	const ucs2_char_t* path_to_playlist,
	const ucs2_char_t* path_to_root,
	const ucs2_char_t* path_to_music,
	const ucs2_char_t* input,
	findfile_music_t* ffd_music,
	int flags,
	void *instance,
	easyh10_error_callback error_proc
	)
{
	int error_occurred = 0, num_success = 0;
	int i;

	for (i = 0;i < pl->num_entries;i++) {
		int valid = 1, success = 0;
		ucs2_char_t music_file[MAX_PATH];
		ucs2_char_t *wcstr = ucs2dup(pl->entries[i].filename);

		if (wcstr) {
			ucs2cpy(music_file, wcstr);
			filepathutil_decode(music_file);

			/* Cleat the entry in case of failure. */
			pl->entries[i].filename[0] = 0;

			if (filepathutil_is_relative(music_file)) {
				ucs2_char_t tmp[MAX_PATH*2];

				/* Relative path: convert the path to the absolute path. */
				filepathutil_relative_to_absolute(tmp, path_to_playlist, music_file);
				ucs2cpy(music_file, tmp);
			} else if (music_file[0] == PATHCHAR) {
				ucs2_char_t tmp[MAX_PATH*2];

				/* The path beginning from '\': convert the path to the complete form. */
				filepathutil_relative_to_absolute(tmp, path_to_root, music_file);
				ucs2cpy(music_file, tmp);
			}

			filepathutil_decode(music_file);

			/* Absolute path: check if the file exists in the H10 drive. */
			if (!filepathutil_is_same_root(music_file, path_to_root)) {
				valid = 0;	/* Set the error flag. */
			}

			/* Make sure that the music file exists. */
			if (!filepathutil_file_exists(music_file)) {
				valid = 0;	/* Set the error flag. */
			}

			/* Guess the missing music file if indicated. */
			if (!valid && (flags & EASYH10_PLAYLIST_GUESS_MISSING)) {
				if (guess_missing_music(music_file, path_to_music) == 0) {
					valid = 1;	/* Guessing succeeded: cancel the error flag. */
				}
			}

			/* Search for the missing music files in music folders if indicated. */
			if (!valid && (flags & EASYH10_PLAYLIST_SEARCH_MISSING)) {
				if (search_missing_music(music_file, path_to_music, ffd_music) == 0) {
					valid = 1;	/* Search succeeded: cancel the error flag. */
				}
			}

			/* Strip the root. */
			if (valid) {
				const ucs2_char_t* pathname = filepathutil_skiproot(music_file, path_to_root);
				if (pathname) {
					ucs2_char_t* ucs2 = ucs2dup(pathname);
					if (ucs2) {
						ucs2cpy(pl->entries[i].filename, ucs2);
						success = 1;
						ucs2free(ucs2);
					}
				}
			}

			if (success) {
				num_success++;
			} else {
				if (error_occurred == 0) {
					error_proc(instance, EASYH10_WARNING_PLAYLIST_CONVERT, (void*)input);
					error_occurred = 1;
				}
				error_proc(instance, EASYH10_WARNING_PLAYLIST_CONVERT_ENTRY, wcstr);
			}
		}

		ucs2free(wcstr);
	}
	return num_success;
}

void generate_plp_filename(ucs2_char_t* dst, const ucs2_char_t* extension, const ucs2_char_t* src, const ucs2_char_t* path_to_playlist, int playlist_in_music)
{
	if (playlist_in_music) {
		int i = 1;

		/* Playlist files outside the "Playlist" folder. */
		ucs2_char_t filename[MAX_PATH], basename[MAX_PATH];

		/* Extract file name part from the source playlist. */
		ucs2cpy(filename, src);
		filepathutil_strippath(filename);
		filepathutil_remove_extension(filename);

		/* Combine the path to the "Playlist" folder. */
		filepathutil_combinepath(basename, sizeof(basename), path_to_playlist, filename);

		/* Generate the output filename. */
		ucs2cpy(dst, basename);
		ucs2cat(dst, extension);
	} else {
		/* Playlist files in the "Playlist" folder. */
		ucs2_char_t filename[MAX_PATH];
		ucs2cpy(filename, src);
		filepathutil_remove_extension(filename);
		ucs2cat(filename, extension);
		ucs2cpy(dst, filename);
	}
}

static int found_file_playlist(void *instance, const ucs2_char_t* found_path, const ucs2_char_t* found_file)
{
	findfile_playlist_dat_t* ffd = (findfile_playlist_dat_t*)instance;

	if (filepathutil_hasext(found_file, ucs2cs_m3u) || 
		filepathutil_hasext(found_file, ucs2cs_m3u8) ||
		filepathutil_hasext(found_file, ucs2cs_pls) ||
		(ffd->regex_playlist && filepathutil_hasext(found_file, ucs2cs_rep))
		) {
		/* Playlist (M3U/M3U8/PLS) file. */
		playlist_target_file_t* new_target = NULL;

		/* Expand the playlist array. */
		ffd->playlists = (playlist_target_file_t*)realloc(
			ffd->playlists,
			sizeof(playlist_target_file_t) * (ffd->num_playlists+1)
			);

		/* new target. */
		new_target = &ffd->playlists[ffd->num_playlists++];

		/* Set path and file. */
		ucs2cpy(new_target->path, found_path);
		ucs2cpy(new_target->file, found_file);
		new_target->playlist_in_music = ffd->playlist_in_music;
		new_target->update = 1;

	} else {
		/* Exit if the filename looks playlist. */
		return 0;
	}

	/* Report progress. */
	if (ffd->proc) {
		ffd->proc(
			ffd->instance,
			EASYH10P_ENUMTARGETS_FILE,
			ffd->num_playlists,
			(void*)(const ucs2_char_t*)found_file
			);
	}

	return 0;
}

static int found_file_music(void *instance, const ucs2_char_t* found_path, const ucs2_char_t* found_file)
{
	static const ucs2_char_t ucs2cs_mp3[] = {'.','m','p','3',0};
	static const ucs2_char_t ucs2cs_wma[] = {'.','w','m','a',0};
	static const ucs2_char_t ucs2cs_wav[] = {'.','w','a','v',0};
	findfile_music_t* ffd = (findfile_music_t*)instance;

	if (filepathutil_hasext(found_file, ucs2cs_mp3) ||
		filepathutil_hasext(found_file, ucs2cs_wma) ||
		filepathutil_hasext(found_file, ucs2cs_wav)) {
		/* Music (MP3/WMA/WAV) file. */
		media_target_file_t* new_target = NULL;

		/* Expand the target array. */
		ffd->targets = (media_target_file_t*)realloc(
			ffd->targets,
			sizeof(media_target_file_t) * (ffd->num_targets+1)
			);
		new_target = &ffd->targets[ffd->num_targets++];

		/* Set path and file. */
		ucs2cpy(new_target->path, found_path);
		ucs2cpy(new_target->file, found_file);
	} else {
		/* Exit if the filename does not have .mp3 or .wma */
		return 0;
	}

	/* Report progress. */
	if (ffd->proc) {
		ffd->proc(
			ffd->instance,
			EASYH10P_ENUMTARGETS_FILE,
			ffd->num_targets,
			(void*)(const ucs2_char_t*)found_file
			);
	}

	return 0;
}

static void copy_music_list(findfile_music_t* dst, const findfile_music_t* src)
{
	int i;

	memset(dst, 0, sizeof(*dst));
	dst->num_targets = src->num_targets;
	dst->targets = calloc(dst->num_targets, sizeof(media_target_file_t));

	for (i = 0;i < dst->num_targets;++i) {
		ucs2cpy(dst->targets[i].path, src->targets[i].path);
		ucs2cpy(dst->targets[i].file, src->targets[i].file);
	}
}

static int generate_regex_playlist(playlist_t* pl, const ucs2_char_t *filename, findfile_music_t* musics)
{
	int i = 0;
	regex_playlist_t* rep = NULL;

	/* Initialize an empty playlist. */
	playlist_init(pl);

	/* Initialize a regex playlist engine. */
	regex_playlist_init(&rep);
	if (!rep) {
		return -1;
	}

	/* Read a regex playlist file. */
	if (regex_playlist_read(rep, filename) != 0) {
		return -1;
	}
	
	/* Search for music files that matches the condition. */
	for (i = 0;i < musics->num_targets;++i) {
		ucs2_char_t str[MAX_PATH];
		ucs2cpy(str, musics->targets[i].path);
		ucs2cat(str, musics->targets[i].file);
		filepathutil_decode(str);
		if (regex_playlist_test(rep, str)) {
			playlist_add(pl, str);
		}
	}
	

	/* Free the regex playlist engine. */
	regex_playlist_finish(rep);
	
	return 0;
}

static const ucs2_char_t* check_playlist_for_shuffle(const ucs2_char_t* filename)
{
	static const ucs2_char_t ucs2cs_sfl[] = {'.','s','f','l'};
	const ucs2_char_t *p = NULL, *q = NULL, *r = NULL;

	q = filename + ucs2len(filename);
	while (filename <= q) {
		if (*q == '.') {
			break;
		}
		--q;
	}
	if (q < filename) {
		return NULL;
	}
	for (p = filename;p < q;++p) {
		if (*p == '.') {
			r = p;
		}
	}

	if (r && ucs2ncmp(r, ucs2cs_sfl, 4) == 0) {
		return r;
	}

	return NULL;
}

static int dummy_progress_callback(void *instance, int phase, int param_int, void *param_ptr)
{
	return 0;
}

static void dummy_error_callback(void *instance, int code, void *param)
{
}


int easyh10_playlist(
	const ucs2_char_t* path_to_root,
	const ucs2_char_t* path_to_music,
	const ucs2_char_t* path_to_playlist,
	const ucs2_char_t* playlist_extension,
	int flags,
	void *instance,
	update_progress_callback progress_proc,
	easyh10_error_callback error_proc
	)
{
	int i, ret, num_to_update, num_updates, num_success;
	findfile_playlist_dat_t ffd;
	findfile_music_t ffd_music, ffd_music_for_regex;
	ucs2_char_t *p = NULL;

	/* If no target specified, exit. */
	if (!(flags & (EASYH10_PLAYLIST_PLAYLIST | EASYH10_PLAYLIST_MUSIC))) {
		return 0;
	}

	/* Set dummy progress callback if progress_proc was NULL. */
	if (!progress_proc) {
		progress_proc = dummy_progress_callback;
	}

	/* Set dummy error callback if error_proc was NULL. */
	if (!error_proc) {
		error_proc = dummy_error_callback;
	}

	memset(&ffd_music_for_regex, 0, sizeof(ffd_music_for_regex));

	/* For progress report during enumeration. */
	ffd.num_playlists = 0;
	ffd.playlists = NULL;
	ffd.instance = instance;
	ffd.proc = progress_proc;
	ffd.regex_playlist = (flags & EASYH10_PLAYLIST_REGEX);

	/* Notify the start of playlist enumeration. */
	progress_proc(instance, EASYH10P_ENUMTARGETS_START, -1, NULL);

	/* Enumerate playlist files in Playlist folder. */
	if (flags & EASYH10_PLAYLIST_PLAYLIST) {
		ffd.playlist_in_music = 0;
		find_file(path_to_playlist, 0, found_file_playlist, &ffd);
	}

	/* Enumerate playlist files under Music folders. */
	if (flags & EASYH10_PLAYLIST_MUSIC) {
		ffd.playlist_in_music = 1;
		find_file(path_to_music, 1, found_file_playlist, &ffd);
	}

	/* Notify the end of playlist enumeration. */
	progress_proc(instance, EASYH10P_ENUMTARGETS_END, ffd.num_playlists, NULL);

	ffd_music.num_targets = 0;
	ffd_music.targets = NULL;
	ffd_music.instance = instance;
	ffd_music.proc = progress_proc;

	if ((flags & EASYH10_PLAYLIST_SEARCH_MISSING) || (flags & EASYH10_PLAYLIST_REGEX)) {
		progress_proc(instance, EASYH10P_PLAYLIST_ENUMMUSIC_START, -1, NULL);
		find_file(path_to_music, 1, found_file_music, &ffd_music);
		if (ffd_music.targets) {
			if (flags & EASYH10_PLAYLIST_REGEX) {
				/* We need to sort music files in an alphabetical order of path/file names. */
				copy_music_list(&ffd_music_for_regex, &ffd_music);
			}
			if (flags & EASYH10_PLAYLIST_SEARCH_MISSING) {
				/* We need to sort music files in an alphabetical order of file names for binary search. */
				qsort(
					ffd_music.targets,
					ffd_music.num_targets,
					sizeof(ffd_music.targets[0]),
					comp_filename
					);
			}
		}
		progress_proc(instance, EASYH10P_PLAYLIST_ENUMMUSIC_END, ffd_music.num_targets, NULL);
	}

	/* Determine the targets to be converted. */
	if (flags & EASYH10_PLAYLIST_INCREMENTAL) {
		num_to_update = 0;
		for (i = 0;i < ffd.num_playlists;i++) {
			ucs2_char_t input[MAX_PATH], output[MAX_PATH];
			playlist_target_file_t* target = &ffd.playlists[i];
			int reconvert = 0;

			/* Generate the input filename. */
			filepathutil_combinepath(input, MAX_PATH, target->path, target->file);

			/* Check if this playlist will be shuffled. */
			if (flags & EASYH10_PLAYLIST_AUTO_SHUFFLE) {
				reconvert |= (check_playlist_for_shuffle(input) ? 1 : 0);
			}

			/* Check if this playlist is regex playlist. */
			if ((flags & EASYH10_PLAYLIST_REGEX) && filepathutil_hasext(input, ucs2cs_rep)) {
				reconvert |= 1;
			}

			/* Make sure to convert shuffling playlist every time. */
			if (!reconvert) {
				/* Generate the output filename. */
				generate_plp_filename(output, playlist_extension, input, path_to_playlist, target->playlist_in_music);
				if (filepathutil_file_exists(output) && filepathutil_compare_lastupdate(input, output) <= 0) {
					target->update = 0;
					continue;
				}
			}
			num_to_update++;
		}
	} else {
		num_to_update = ffd.num_playlists;
	}

	/* Notify the number of playlist conversion. */
	progress_proc(instance, EASYH10P_PLAYLIST_START, num_to_update, NULL);

	/* Convert playlists. */
	num_updates = 0;
	num_success = 0;
	for (i = 0;i < ffd.num_playlists;i++) {
		playlist_t pl;
		ucs2_char_t input[MAX_PATH], output[MAX_PATH];
		playlist_target_file_t* target = &ffd.playlists[i];

		if (target->update) {
			progress_proc(instance, EASYH10P_PLAYLIST, ++num_updates, (void*)(const ucs2_char_t*)target->file);

			/* Initialize the playlist structure. */
			playlist_init(&pl);

			/* Generate the input filename. */
			filepathutil_combinepath(input, MAX_PATH, target->path, target->file);

			/* Read the playlist. */
			if (filepathutil_hasext(target->file, ucs2cs_m3u)) {
				if (playlist_m3u_read(&pl, input) != 0) {
					error_proc(instance, EASYH10_WARNING_PLAYLIST_READ, input);
					continue;
				}
			} else if (filepathutil_hasext(target->file, ucs2cs_m3u8)) {
				if (playlist_m3u8_read(&pl, input) != 0) {
					error_proc(instance, EASYH10_WARNING_PLAYLIST_READ, input);
					continue;
				}
			} else if (filepathutil_hasext(target->file, ucs2cs_pls)) {
				if (playlist_pls_read(&pl, input) != 0) {
					error_proc(instance, EASYH10_WARNING_PLAYLIST_READ, input);
					continue;
				}
			} else if ((flags & EASYH10_PLAYLIST_REGEX) && filepathutil_hasext(target->file, ucs2cs_rep)) {
				if (generate_regex_playlist(&pl, input, &ffd_music_for_regex) != 0) {
					error_proc(instance, EASYH10_WARNING_PLAYLIST_READ, input);
					continue;
				}
			}
			
			/* Normalize the content of the playlist. */
			ret = normalize_playlist(
				&pl,
				target->path,
				path_to_root,
				path_to_music,
				input,
				&ffd_music,
				flags,
				instance,
				error_proc
				);
			if (ret == pl.num_entries || (ret > 0 && (flags & EASYH10_PLAYLIST_SKIP_MISSING))) {
				const ucs2_char_t* shuffle = NULL;
				
				if (flags & EASYH10_PLAYLIST_AUTO_SHUFFLE) {
					shuffle = check_playlist_for_shuffle(input);
				}

				/* Generate the output filename. */
				generate_plp_filename(output, playlist_extension, input, path_to_playlist, target->playlist_in_music);

				if (shuffle) {
					playlist_shuffle(&pl);
				}

				/* Write the playlist. */
				if (pl.num_entries > 0) {
					if (playlist_plp_write(&pl, output) != 0) {
						error_proc(instance, EASYH10_WARNING_PLAYLIST_WRITE, output);
					} else {
						num_success++;
					}
				}
			}

			playlist_finish(&pl);
		}
	}

	/* Notify the end of playlist conversion. */
	progress_proc(instance, EASYH10P_PLAYLIST_END, num_success, NULL);

	free(ffd_music_for_regex.targets);
	free(ffd_music.targets);
	free(ffd.playlists);
	return 0;
}
