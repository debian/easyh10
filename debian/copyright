Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: EasyH10
Upstream-Contact: Nyaochi <nyaochi@users.sf.net>
Source: https://easyh10.sourceforge.net/

Files: *
Copyright: 2005      Toby Corkindale (iRiver.pm)
           2005-2006 Nyaochi <nyaochi@users.sf.net>
License: GPL-2+
Comment: alternative email address from README file:
         Nyaochi <nyaochi1nyaochi2sakura3ne4jp>
         Replace '1'->'@', '2'->'.', '3'->'.', '4'->'.' respectively)

Files: common/rel2abs.c
Copyright: 1997 Shigio Yamaguchi
           1999 Tama Communications Corporation
License: BSD-2-Clause

Files: cui/getopt.c
       cui/getopt1.c
Copyright: 1987-1999 Free Software Foundation, Inc
License: LGPL-2+

Files: libh10db/crc.c
       libh10db/crc.h
Copyright: 2000 Michael Barr
License: public-domain

Files: debian/*
Copyright: 2005-2006 Benjamin Seidenberg <astronut@dlgeek.net>
           2016      Allan Dixon Jr. <allandixonjr@gmail.com>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: BSD-2-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 SUCH DAMAGE.

License: LGPL-2+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in "/usr/share/common-licenses/LGPL-2".

License: public-domain
 This software is placed into the public domain and may be used for any
 purpose. However, this notice must not be changed or removed and no
 warranty is either expressed or implied by its publication or distribution.
