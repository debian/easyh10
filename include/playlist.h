/*
 *      M3U/M3U8/PLS playlist routine.
 *
 *      Copyright (c) 2005 Nyaochi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA, or visit
 * http://www.gnu.org/copyleft/gpl.html .
 *
 */

/* $Id: playlist.h,v 1.4 2006/01/23 01:56:00 nyaochi Exp $ */

#ifndef	__PLAYLIST_H__
#define	__PLAYLIST_H__

#ifdef	__cplusplus
extern "C" {
#endif/*__cplusplus*/

struct tag_playlist_entry {
	ucs2_char_t filename[MAX_PATH];
	int order;
};
typedef struct tag_playlist_entry playlist_entry_t;

struct tag_playlist {
	int num_entries;
	playlist_entry_t* entries;
};
typedef struct tag_playlist playlist_t;

void playlist_init(playlist_t* pl);
void playlist_finish(playlist_t *pl);

int playlist_m3u_read(playlist_t* pl, const ucs2_char_t *filename);
int playlist_m3u8_read(playlist_t* pl, const ucs2_char_t *filename);
int playlist_pls_read(playlist_t* pl, const ucs2_char_t *filename);
int playlist_plp_write(playlist_t* pl, const ucs2_char_t *filename);
void playlist_shuffle(playlist_t* pl);

int playlist_add(playlist_t* pl, const ucs2_char_t* filename);

#ifdef	__cplusplus
}
#endif/*__cplusplus*/

#endif/*__PLAYLIST_H__*/
