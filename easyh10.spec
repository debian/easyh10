%define name easyh10
%define version 1.5
%define release 1

Summary: Utility to manage the iRiver H10 music player
Name: %{name}
Version: %{version}
Release: %{release}
Source0: %{name}-%{version}.tar.gz
Vendor: The EasyH10 project
Copyright: GPL
Group: Applications/Multimedia
URL: http://easyh10.sourceforge.net/

Requires: libid3tag
BuildRequires: libid3tag-devel
BuildRoot: %{_tmppath}/%{name}-%{version}-root

%description
Easyh10 is a command-line utility to generate a media database and
playlist for an iRiver H10 digital audio player. 
Transferring the actual music files must be done manually.

%prep

%setup


%build
%configure
%__make
#./configure --prefix=/usr
#make

%install
rm -rf $RPM_BUILD_ROOT
%makeinstall
#make DESTDIR=$RPM_BUILD_ROOT install


%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%doc AUTHORS COPYING ChangeLog INSTALL NEWS README
%{_prefix}/bin/easyh10
%{_mandir}/man1/easyh10.1*
%{_datadir}/easyh10/

%changelog
* Mon Nov 14 2005 Nyaochi <nyaochi@users.sourceforge.net> 1.2.1-1
- Imported this file into the CVS tree as "easyh10.spec.in"

* Sun Nov 13 2005 KATO Masaya <cachu@cachu.xrea.jp> 1.2-1
- change the description of RPM package

* Wed Nov 2 2005 KATO Masaya <cachu@cachu.xrea.jp> 1.2-0
- new upstream

* Fri Oct 7 2005 KATO Masaya <cachu@cachu.xrea.jp> 1.0b8-0
- new upstream

* Sun Aug 28 2005 KATO Masaya <cachu@cachu.xrea.jp> 1.0b6-0
- new upstream

* Sun Aug 28 2005 KATO Masaya <cachu@cachu.xrea.jp> 1.0b5-0
- initial build.


# end of file
