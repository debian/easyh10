/*
 *      H10DB.hdr parser and writer.
 *
 *      Copyright (c) 2005 Nyaochi
 *		Copyright (c) 2005 Toby Corkindale (iRiver.pm).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA, or visit
 * http://www.gnu.org/copyleft/gpl.html .
 *
 */

/* $Id: h10db_hdr.c,v 1.13 2005/10/20 17:13:09 nyaochi Exp $ */

#ifdef	HAVE_CONFIG_H
#include <config.h>
#endif/*HAVE_CONFIG_H*/

#include <os.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ucs2char.h>
#include "bufferedfile.h"
#include "serialize.h"

#include <h10db.h>

#define	MAXELEM(x)		(sizeof(x) / sizeof(x[0]))



static void fprints(FILE *fp, const char *format, const ucs2_char_t* value)
{
	char *mbs = ucs2dupmbs(value);
	if (mbs) {
		fprintf(fp, format, mbs);
		ucs2free(mbs);
	}
}




/*
 * Field descriptor.
 */
void h10db_fd_init(h10db_fd_t* hdr_idx)
{
	memset(hdr_idx, 0, sizeof(*hdr_idx));
}

void h10db_fd_finish(h10db_fd_t* hdr_idx)
{
	h10db_fd_init(hdr_idx);
}

int h10db_fd_serialize(struct bfile *bfp, h10db_fd_t* h10fd, int is_storing)
{
	int ret = 0;
	ret |= serialize_uint32(bfp, &h10fd->id, is_storing);
	ret |= serialize_uint32(bfp, &h10fd->field_type, is_storing);
	ret |= serialize_uint32(bfp, &h10fd->max_length, is_storing);
	ret |= serialize_uint32(bfp, &h10fd->unknown4, is_storing);
	ret |= serialize_uint32(bfp, &h10fd->unknown5, is_storing);
	ret |= serialize_uint32(bfp, &h10fd->has_index, is_storing);
	ret |= serialize_uint32(bfp, &h10fd->unknown6, is_storing);
	ret |= serialize_uint32(bfp, &h10fd->unknown7, is_storing);
	ret |= serialize_ucs2le_fixed(bfp, h10fd->index_pathname, H10DB_PATHLENGTH, is_storing);
	return ret;
}

void h10db_fd_repr(FILE *fp, const h10db_fd_t* h10fd)
{
	fprintf(fp, "id: 0x%08X\n", h10fd->id);
	fprintf(fp, "field_type: 0x%08X\n", h10fd->field_type);
	fprintf(fp, "max_length: %d\n", h10fd->max_length);
	fprintf(fp, "unknown4: %d\n", h10fd->unknown4);
	fprintf(fp, "unknown5: %d\n", h10fd->unknown5);
	fprintf(fp, "has_index: %d\n", h10fd->has_index);
	fprintf(fp, "unknown6: %d\n", h10fd->unknown6);
	fprintf(fp, "unknown7: %d\n", h10fd->unknown7);
	fprints(fp, "index_pathname: %s\n", h10fd->index_pathname);
}

void h10db_fd_repr2(FILE *fp, const h10db_fd_t* h10fd)
{
	fprintf(fp, "{");
	fprintf(fp, "0x%08X, ", h10fd->id);
	fprintf(fp, "%d, ", h10fd->field_type);
	fprintf(fp, "%3d, ", h10fd->max_length);
	fprintf(fp, "%d, ", h10fd->unknown4);
	fprintf(fp, "%d, ", h10fd->unknown5);
	fprintf(fp, "%d, ", h10fd->has_index);
	fprintf(fp, "%d, ", h10fd->unknown6);
	fprintf(fp, "%d, ", h10fd->unknown7);
	fprints(fp, "L\"%s\"", h10fd->index_pathname);
	fprintf(fp, "}");
}






/*
 *	H10DB.hdr
 */
void h10db_hdr_init(h10db_hdr_t* hdr, uint32_t max_entries, uint32_t padding_size, uint32_t has_unknown5)
{
	memset(hdr, 0, sizeof(*hdr));
	hdr->dat_field_offset = calloc(max_entries, sizeof(h10db_field_offsets_t));
	hdr->dat_entry_offset = calloc(max_entries+1, sizeof(uint32_t));
	hdr->max_entries = max_entries;
	hdr->padding_size = padding_size;
	hdr->has_unknown5 = has_unknown5;
}

void h10db_hdr_finish(h10db_hdr_t* hdr)
{
	free(hdr->dat_entry_offset);
	free(hdr->dat_field_offset);
	memset(hdr, 0, sizeof(*hdr));
}

int h10db_hdr_serialize(struct bfile *bfp, h10db_hdr_t* hdr, int is_storing, int is_template)
{
	int i, j, ret = 0;

	/* Clear these field for template generation. */
	if (is_template && is_storing) {
		hdr->num_dat_entries = 0;
		hdr->num_dat_inactive_entries = 0;
	}

	ret |= serialize_uint32(bfp, &hdr->unknown1, is_storing);
	ret |= serialize_uint32(bfp, &hdr->unknown2, is_storing);
	ret |= serialize_ucs2le_fixed(bfp, hdr->pathname_dat, H10DB_PATHLENGTH, is_storing);
	ret |= serialize_uint32(bfp, &hdr->unknown3, is_storing);
	ret |= serialize_ucs2le_fixed(bfp, hdr->pathname_hdr, H10DB_PATHLENGTH, is_storing);
	ret |= serialize_uint32(bfp, &hdr->unknown4, is_storing);
	ret |= serialize_uint32(bfp, &hdr->num_dat_entries, is_storing);
	ret |= serialize_uint32(bfp, &hdr->num_dat_inactive_entries, is_storing);
	ret |= serialize_uint32(bfp, &hdr->num_dat_fields, is_storing);

	for (i = 0;i < MAXELEM(hdr->fd);i++) {
		ret |= h10db_fd_serialize(bfp, &hdr->fd[i], is_storing);
	}

	if (is_storing) {
		uint32_t i;
		for (i = 0;i < hdr->padding_size;i++) {
			static const uint8_t c = 0;
			ret |= (bf_write(bfp, &c, sizeof(c)) == sizeof(c) ? 0 : 1);
		}
	} else {
		uint32_t i;
		for (i = 0;i < hdr->padding_size;i++) {
			uint8_t c = 0;
			ret |= (bf_read(bfp, &c, sizeof(c)) == sizeof(c) ? 0 : 1);
		}
	}

	for (i = 0;i < MAXELEM(hdr->max_dat_field_size);i++) {
		ret |= serialize_uint32(bfp, &hdr->max_dat_field_size[i], is_storing);
	}

	ret |= serialize_uint32(bfp, &hdr->dat_size, is_storing);

	if (hdr->has_unknown5) {
		ret |= serialize_uint32(bfp, &hdr->unknown5, is_storing);
	}

	if (!is_template) {
		for (i = 0;i < hdr->max_entries;i++) {
			for (j = 0;j < H10DB_NUM_DAT_FIELDS;j++) {
				ret |= serialize_uint16(bfp, &hdr->dat_field_offset[i][j], is_storing);
			}
		}
		for (i = 0;i < hdr->max_entries+1;i++) {
			ret |= serialize_uint32(bfp, &hdr->dat_entry_offset[i], is_storing);
		}
	}

	return ret;
}

void h10db_hdr_repr(FILE *fp, const h10db_hdr_t* hdr)
{
	int i, j;

	fprintf(fp, "# H10DB.hdr\n");
	fprintf(fp, "unknown1: %d\n", hdr->unknown1);
	fprintf(fp, "unknown2: %d\n", hdr->unknown2);
	fprints(fp, "pathname_dat: %s\n", hdr->pathname_dat);
	fprintf(fp, "unknown3: %d\n", hdr->unknown3);
	fprints(fp, "pathname_hdr: %s\n", hdr->pathname_hdr);
	fprintf(fp, "unknown4: 0x%08X\n", hdr->unknown4);
	fprintf(fp, "num_dat_entries: %d\n", hdr->num_dat_entries);
	fprintf(fp, "num_dat_inactive_entries: %d\n", hdr->num_dat_inactive_entries);
	fprintf(fp, "num_dat_fields: %d\n", hdr->num_dat_fields);

/*
	for (i = 0;i < MAXELEM(hdr->fd);i++) {
		fprintf(fp, "## H10DB.hdr::field_descriptor[%d]\n", i);
		h10db_fd_repr(fp, &hdr->fd[i]);
		fprintf(fp, "## End of H10DB.hdr::field_descriptor[%d]\n", i);
	}
*/

	for (i = 0;i < MAXELEM(hdr->fd);i++) {
		fprintf(fp, "fd[%2d]: ", i);
		h10db_fd_repr2(fp, &hdr->fd[i]);
		fprintf(fp, "\n");
	}

	for (i = 0;i < MAXELEM(hdr->max_dat_field_size);i++) {
		fprintf(fp, "max_dat_field_size[%d]: %d\n", i, hdr->max_dat_field_size[i]);
	}

	fprintf(fp, "dat_size: %d\n", hdr->dat_size);
	if (hdr->has_unknown5) {
		fprintf(fp, "unknown5: %d\n", hdr->unknown5);
	}

	for (i = 0;i < hdr->max_entries;i++) {
		fprintf(fp, "dat_field_offset[%d]: {", i);
		for (j = 0;j < H10DB_NUM_DAT_FIELDS;j++) {
			if (j != 0) {
				fprintf(fp, ",");
			}
			fprintf(fp, "%3d", hdr->dat_field_offset[i][j]);
		}
		fprintf(fp, "}\n");
	}

	for (i = 0;i < hdr->max_entries+1;i++) {
		fprintf(fp, "dat_entry_offset[%d]: %d\n", i, hdr->dat_entry_offset[i]);
	}
	fprintf(fp, "# End of H10DB.hdr\n");
}

