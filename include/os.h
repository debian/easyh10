#ifndef	__OS_H__
#define	__OS_H__

#include <stdio.h>

#if		defined(WIN32) || defined(OS2)
typedef unsigned char	uint8_t;
typedef unsigned int	uint32_t;
typedef unsigned short	uint16_t;

#elif	defined(bsdi) || defined(FREEBSD) || defined(OPENBSD)
#include <sys/types.h>

#else	
#ifdef HAVE_STDINT_H
#include <stdint.h>
#endif
#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif
#endif

#if		defined(WIN32)
#define	snprintf	_snprintf
#define snwprintf	_snwprintf
#define	getcwd		_getcwd

#define	PATHCHAR	'\\'
#define	PATHSTR		"\\"

#else

#define	PATHCHAR	'/'
#define	PATHSTR		"/"

#endif

#if		defined(WIN32)
#include <windows.h>

#elif	defined(bsdi) || defined(FREEBSD) || defined(OPENBSD)
#include <sys/param.h>
#define	MAX_PATH	MAXPATHLEN

#else
#include <limits.h>
#define	MAX_PATH	PATH_MAX

#endif

#endif/*__OS_H__*/
