/*
 *      UCS-2 character set library.
 *
 *      Copyright (c) 2005 Nyaochi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA, or visit
 * http://www.gnu.org/copyleft/gpl.html .
 *
 */

/* $Id: ucs2char.h,v 1.30 2006/04/30 11:59:35 nyaochi Exp $ */

#ifndef	__UCS2CHAR_H__
#define	__UCS2CHAR_H__

#include <stdio.h>
#include <time.h>

#ifdef	__cplusplus
extern "C" {
#endif/*__cplusplus*/

typedef uint16_t ucs2_char_t;

struct tag_ucs2conv {
  const char *from;
  const char *to;
};
typedef struct tag_ucs2conv ucs2conv_t;

int ucs2init(const char *encoding);
int ucs2check(ucs2conv_t* conv);

int ucs2set_encoding(const char *encoding, ucs2conv_t* conv);
int ucs2set_encoding_music(const char *encoding, ucs2conv_t* conv);
void ucs2set_codepage(int cp);

void ucs2big2little(ucs2_char_t* value);


void *ucs2malloc(size_t size);
void *ucs2realloc(void *ptr, size_t size);
void ucs2free(void* str);

int is_ucs2surrogate(ucs2_char_t c);
int isucs2space(ucs2_char_t c);
int isucs2digit(ucs2_char_t c);

size_t ucs2len(const ucs2_char_t* string);
ucs2_char_t* ucs2cpy(ucs2_char_t* dst, const ucs2_char_t* src);
ucs2_char_t* ucs2ncpy(ucs2_char_t* dst, const ucs2_char_t* src, size_t count);
ucs2_char_t* ucs2cat(ucs2_char_t* dst, const ucs2_char_t* src);

ucs2_char_t* ucs2chr(const ucs2_char_t* string, ucs2_char_t c);
ucs2_char_t* ucs2rchr(const ucs2_char_t* string, ucs2_char_t c);

ucs2_char_t* ucs2str(const ucs2_char_t* str, const ucs2_char_t* search);

ucs2_char_t ucs2lower(ucs2_char_t c);
ucs2_char_t* ucs2lwr(ucs2_char_t* str);

ucs2_char_t ucs2upper(ucs2_char_t c);
ucs2_char_t* ucs2upr(ucs2_char_t* str);

ucs2_char_t* ucs2dup(const ucs2_char_t* src);
ucs2_char_t* ucs2ndup(const ucs2_char_t* src, size_t length);

ucs2_char_t* ucs2strip(ucs2_char_t* str);

int ucs2cmp(const ucs2_char_t* x, const ucs2_char_t* y);
int ucs2ncmp(const ucs2_char_t* x, const ucs2_char_t* y, size_t n);
int ucs2icmp(const ucs2_char_t* x, const ucs2_char_t* y);

ucs2_char_t* ucs2righttoleft_encode(ucs2_char_t* str);
ucs2_char_t* ucs2righttoleft_decode(ucs2_char_t* str);

int ucs2toi(const ucs2_char_t* str);
ucs2_char_t* itoucs2(int value, ucs2_char_t *string, int radix);


size_t ucs2tombs(char *mbstr, size_t mbs_size, const ucs2_char_t *ucs2str, size_t ucs_size);
size_t mbstoucs2(ucs2_char_t *ucs2str, size_t ucs_size, const char *mbstr, size_t mbs_size);
size_t mbstoucs2_music(ucs2_char_t *ucs2str, size_t ucs_size, const char *mbstr, size_t mbs_size);
size_t utf8toucs2(ucs2_char_t *ucs2str, size_t ucs_size, const char *mbstr, size_t mbs_size);
size_t ucs2toutf8(char *mbstr, size_t mbs_len, const ucs2_char_t *ucs2str, size_t ucs_len);

ucs2_char_t* mbsdupucs2(const char *mbstr);
ucs2_char_t* mbsdupucs2_music(const char *mbstr);
ucs2_char_t* utf8dupucs2(const char *utf8str);
char *ucs2duputf8(const ucs2_char_t* ucs2str);
char *ucs2dupmbs(const ucs2_char_t *ucs2str);

#ifdef	_MSC_VER	/* We can't seem to pass FILE pointer beyond a thread boundary. */
static FILE *ucs2fopen(const ucs2_char_t *filename, const char *mode)
{
	FILE *fp = NULL;
	ucs2_char_t *ucs2_mode = mbsdupucs2(mode);
	if (ucs2_mode) {
		fp = _wfopen(filename, ucs2_mode);
		ucs2free(ucs2_mode);
	}
	return fp;
}
#else
FILE *ucs2fopen(const ucs2_char_t *filename, const char *mode);
#endif

time_t ucs2stat_mtime(const ucs2_char_t *filename);
uint32_t ucs2stat_size(const ucs2_char_t *filename);
int ucs2stat_is_dir(const ucs2_char_t *filename);
int ucs2stat_is_exist(const ucs2_char_t *filename);


#ifdef	__cplusplus
}
#endif/*__cplusplus*/

#endif/*__UCS2CHAR_H__*/
