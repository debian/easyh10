/*
 *      H10DB_*.idx parser and writer.
 *
 *      Copyright (c) 2005 Nyaochi
 *		Copyright (c) 2005 Toby Corkindale (iRiver.pm).
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA, or visit
 * http://www.gnu.org/copyleft/gpl.html .
 *
 */

/* $Id: h10db_idx.c,v 1.5 2005/06/03 09:29:56 nyaochi Exp $ */

#ifdef	HAVE_CONFIG_H
#include <config.h>
#endif/*HAVE_CONFIG_H*/

#include <os.h>
#include <memory.h>
#include <stdio.h>
#include <stdlib.h>
#include <ucs2char.h>
#include "bufferedfile.h"
#include "serialize.h"

#include <h10db.h>

void h10db_idx_init(h10db_idx_t* item)
{
	memset(item, 0, sizeof(*item));
}

void h10db_idx_swap(h10db_idx_t* x, h10db_idx_t* y)
{
	h10db_idx_t tmp;
	memcpy(&tmp, x, sizeof(*x));
	memcpy(x, y, sizeof(*y));
	memcpy(y, &tmp, sizeof(tmp));
}

int h10db_idx_serialize(struct bfile *bfp, h10db_idx_t* item, int is_storing)
{
	int ret = 0;
	ret |= serialize_uint32(bfp, &item->status, is_storing);
	ret |= serialize_uint32(bfp, &item->index, is_storing);
	ret |= serialize_uint32(bfp, &item->check_value, is_storing);
	return ret;
}

int h10db_idx_read(struct bfile *bfp, h10db_idx_t** array_ptr, int* num_ptr)
{
	int num = 0;
	h10db_idx_t* array = NULL;

	while (!bf_eof(bfp)) {
		h10db_idx_t item;

		h10db_idx_init(&item);
		if (h10db_idx_serialize(bfp, &item, 0) != 0) {
			if (bf_eof(bfp)) {
				break;
			}
			return H10DBE_IDX_READ;
		}

		array = realloc(array, sizeof(h10db_idx_t) * (num+1));
		if (!array) {
			return H10DBE_OUTOFMEMORY;
		}
		h10db_idx_init(&array[num]);
		h10db_idx_swap(&array[num], &item);
		num++;
	}

	*array_ptr = array;
	*num_ptr = num;
	return 0;
}

int h10db_idx_write(struct bfile *bfp, const h10db_idx_t* array, int num)
{
	int i;
	for (i = 0;i < num;i++) {
		if (h10db_idx_serialize(bfp, (h10db_idx_t*)&array[i], 1) != 0) {
			return H10DBE_IDX_WRITE;
		}
	}
	return 0;
}

void h10db_idx_repr(FILE *fp, const h10db_idx_t* item)
{
	fprintf(fp, "inactive: %d\n", item->status);
	fprintf(fp, "index: %d\n", item->index);
	fprintf(fp, "check_value: 0x%08X\n", item->check_value);
}
