/*
 *      File/path utility implemented with Win32 API.
 *
 *      Copyright (c) 2005 Nyaochi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA, or visit
 * http://www.gnu.org/copyleft/gpl.html .
 *
 */

/* $Id: filepathutil.h,v 1.12 2006/01/22 03:30:12 nyaochi Exp $ */

#ifndef	__FILEPATHUTIL_H__
#define	__FILEPATHUTIL_H__

#include <ucs2char.h>


#ifdef	__cplusplus
extern "C" {
#endif/*__cplusplus*/

typedef int (*filepathutil_findfile_callback)(
	void *instance,
	const ucs2_char_t* found_path,
	const ucs2_char_t* found_file
	);

int find_file(
	const ucs2_char_t* path,
	int recursive,
	filepathutil_findfile_callback callback,
	void *instance
	);

ucs2_char_t* filepathutil_addslash(ucs2_char_t* path);
ucs2_char_t* filepathutil_removeslash(ucs2_char_t* path);

int filepathutil_hasext(const ucs2_char_t* filename, const ucs2_char_t* ext);

const ucs2_char_t* filepathutil_combinepath(ucs2_char_t* dst, size_t size, const ucs2_char_t* path, const ucs2_char_t* file);

void filepathutil_strippath(ucs2_char_t* path);
const ucs2_char_t* filepathutil_skiproot(const ucs2_char_t* path, const ucs2_char_t* root);
const ucs2_char_t* filepathutil_skippath(const ucs2_char_t* path);
const ucs2_char_t* filepathutil_skip_one_directory(const ucs2_char_t* path);

void filepathutil_add_extension(ucs2_char_t* path, const ucs2_char_t* ext);
void filepathutil_remove_extension(ucs2_char_t* path);
void filepathutil_remove_filespec(ucs2_char_t* path);
void filepathutil_replace_slash(ucs2_char_t* path, ucs2_char_t c);
void filepathutil_quote_spaces(ucs2_char_t* path);

int filepathutil_is_relative(const ucs2_char_t* path);
int filepathutil_relative_to_absolute(ucs2_char_t* absolute, const ucs2_char_t* base, const ucs2_char_t* relative);
int filepathutil_path_canonicalize(ucs2_char_t* dst, const ucs2_char_t* src);
int filepathutil_file_exists(const ucs2_char_t* path);
int filepathutil_is_same_root(const ucs2_char_t* path1, const ucs2_char_t* path2);

int filepathutil_compare_lastupdate(const ucs2_char_t* file1, const ucs2_char_t* file2);

int filepathutil_copyfile(const ucs2_char_t* src, const ucs2_char_t* dst);

int filepathutil_encode(ucs2_char_t* path);
int filepathutil_decode(ucs2_char_t* path);

#ifdef	__cplusplus
}
#endif/*__cplusplus*/

#endif/*__FILEPATHUTIL_H__*/
