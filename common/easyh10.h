/*
 *      iriver H10 high-level utility.
 *
 *      Copyright (c) 2005 Nyaochi
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA, or visit
 * http://www.gnu.org/copyleft/gpl.html .
 *
 */

/* $Id: easyh10.h,v 1.31 2006/04/29 05:52:04 nyaochi Exp $ */

#ifndef	__EASYH10_H__
#define	__EASYH10_H__

#ifdef	__cplusplus
extern "C" {
#endif/*__cplusplus*/

enum {
	EASYH10_DATABASE_NONE =						0x00000000,
	EASYH10_DATABASE_NEW =						0x00010000,
	EASYH10_DATABASE_CLEAN =					0x00020000,
	EASYH10_DATABASE_INCREMENTAL =				0x00040000,
	EASYH10_DATABASE_RIGHTTOLEFT =				0x00080000,

	EASYH10_DATABASE_ORDER_LEAVE =				0x00000000,
	EASYH10_DATABASE_ORDER_TRACKNAME =			0x00100000,
	EASYH10_DATABASE_ORDER_FILENAME =			0x00200000,
	EASYH10_DATABASE_ORDER_TRACKNUMBER =		0x00300000,
	EASYH10_DATABASE_ORDER_COMPLETERANDOM =		0x00400000,
	EASYH10_DATABASE_ORDER_ALBUMRANDOM =		0x00500000,
	EASYH10_DATABASE_ORDER_ALLRANDOM =			0x00600000,
	EASYH10_DATABASE_ORDERMASK =				0xFF0FFFFF,
	EASYH10_DATABASE_ORDERUNMASK =				0x00F00000,

	EASYH10_PLAYLIST_NONE =						0x00000000,
	EASYH10_PLAYLIST_PLAYLIST =					0x01000000,
	EASYH10_PLAYLIST_MUSIC =					0x02000000,
	EASYH10_PLAYLIST_INCREMENTAL =				0x04000000,
	EASYH10_PLAYLIST_GUESS_MISSING =			0x08000000,
	EASYH10_PLAYLIST_SEARCH_MISSING =			0x10000000,
	EASYH10_PLAYLIST_SKIP_MISSING =				0x20000000,
	EASYH10_PLAYLIST_AUTO_SHUFFLE =				0x40000000,
	EASYH10_PLAYLIST_REGEX =					0x80000000,

	EASYH10_TUNER_NONE =						0x00000000,
	EASYH10_TUNER_IMPORT =						0x00001000,
	EASYH10_TUNER_EXPORT =						0x00002000,
	EASYH10_TUNER_MERGE =						0x00004000,
	EASYH10_TUNER_CLEAN =						0x00008000,

	EASYH10_FIRMWARE_INTERNATIONAL =			0x00000001,
	EASYH10_FIRMWARE_US =						0x00000002,
	EASYH10_FIRMWARE_MASK =						0xFFFFFFF0,
	EASYH10_FIRMWARE_UNMASK =					0x0000000F,

	EASYH10_CAPACITY_1GB =						0x00000010,
	EASYH10_CAPACITY_2GB =						0x00000020,
	EASYH10_CAPACITY_5GB =						0x00000050,
	EASYH10_CAPACITY_6GB =						0x00000060,
	EASYH10_CAPACITY_20GB =						0x00000140,
	EASYH10_CAPACITY_MASK =						0xFFFFF00F,
	EASYH10_CAPACITY_UNMASK =					0x00000FF0,
};

enum {
	/* From easyh10_database(), easyh10_playlist() */
	EASYH10P_ENUMTARGETS_START,			/*	0					NULL				*/
	EASYH10P_ENUMTARGETS_FILE,			/*	num_targets			found_file (UCS2)	*/
	EASYH10P_ENUMTARGETS_END,			/*	num_targets			NULL				*/

	/* From easyh10_database() */
	EASYH10P_READ_MODEL_START,			/*	0					file (UCS2)			*/
	EASYH10P_READ_MODEL,				/*	progress [%]		NULL				*/
	EASYH10P_READ_MODEL_END,			/*	0					description	(MBS)	*/

	EASYH10P_DB_NUM_IDX,				/*	num_idxes			NULL				*/
	EASYH10P_DB_NUM_EXISTING,			/*	num_existing		NULL				*/
	EASYH10P_DB_NUM_NEW,				/*	num_new				NULL				*/
	EASYH10P_DB_NUM_DISAPPEARED,		/*	num_disappeared		NULL				*/
	EASYH10P_DB_NUM_OBTAIN,				/*	num_to_obtain		NULL				*/
	EASYH10P_DB_NUM_UNUSED,				/*	num_to_remove		NULL				*/

	EASYH10P_READ_START,				/*	0					NULL				*/
	EASYH10P_READ_HDR,					/*	progress [%]		NULL				*/
	EASYH10P_READ_DAT,					/*	progress [%]		NULL				*/
	EASYH10P_READ_IDX,					/*	progress [%]		NULL				*/
	EASYH10P_READ_UPD,					/*	progress [%]		NULL				*/
	EASYH10P_READ_END,					/*	0					NULL				*/

	EASYH10P_WRITE_START,				/*	0					NULL				*/
	EASYH10P_WRITE_IDX,					/*	progress [%]		NULL				*/
	EASYH10P_WRITE_DAT,					/*	progress [%]		NULL				*/
	EASYH10P_WRITE_HDR,					/*	progress [%]		NULL				*/
	EASYH10P_WRITE_UPD,					/*	progress [%]		NULL				*/
	EASYH10P_WRITE_END,					/*	0					NULL				*/

	EASYH10P_GETINFO_START,				/*	num_updates			NULL				*/
	EASYH10P_GETINFO_FILE,				/*	update_index		filename (UCS2)		*/
	EASYH10P_GETINFO_END,				/*	num_updates			NULL				*/

	EASYH10P_UPDATE_START,				/*	0					NULL				*/
	EASYH10P_UPDATE_CLEAN,				/*	0					NULL				*/
	EASYH10P_UPDATE_IDX,				/*	progress [%]		NULL				*/
	EASYH10P_UPDATE_END,				/*	0					NULL				*/

	/* From easyh10_playlist() */
	EASYH10P_PLAYLIST_ENUMMUSIC_START,	/*	0					NULL				*/
	EASYH10P_PLAYLIST_ENUMMUSIC_FILE,	/*	num_targets			found_file (UCS2)	*/
	EASYH10P_PLAYLIST_ENUMMUSIC_END,	/*	num_targets			NULL				*/

	EASYH10P_PLAYLIST_START,			/*	num_to_update		NULL				*/
	EASYH10P_PLAYLIST,					/*	update_index		filename (UCS2)		*/
	EASYH10P_PLAYLIST_END,				/*	num_success			NULL				*/

	/* From easyh10_generate_template() */
	EASYH10P_WRITE_MODEL_START,			/*	0					NULL				*/
	EASYH10P_WRITE_MODEL,				/*	progress [%]		NULL				*/
	EASYH10P_WRITE_MODEL_END,			/*	0					NULL				*/

	/* From easyh10_tuner_import() */
	EASYH10P_TUNER_IMPORT_START,		/*	0					NULL				*/
	EASYH10P_TUNER_IMPORT,				/*	num_presets			NULL				*/
	EASYH10P_TUNER_IMPORT_MERGE,		/*	num_presets			NULL				*/
	EASYH10P_TUNER_IMPORT_END,			/*	0					NULL				*/

	/* From easyh10_tuner_export() */
	EASYH10P_TUNER_EXPORT_START,		/*	0					NULL				*/
	EASYH10P_TUNER_EXPORT,				/*	num_presets			NULL				*/
	EASYH10P_TUNER_EXPORT_MERGE,		/*	num_presets			NULL				*/
	EASYH10P_TUNER_EXPORT_END,			/*	0					NULL				*/
};

enum {
	EASYH10_SUCCESS = 0,

	EASYH10_ERROR_OUTOFMEMORY,				/*	NULL				*/
	EASYH10_ERROR_INITLIBH10DB,				/*	NULL				*/
	EASYH10_ERROR_LOADTEMPLATE,				/*	filename			*/
	EASYH10_ERROR_STORETEMPLATE,			/*	filename			*/
	EASYH10_ERROR_READDB,					/*	filename			*/
	EASYH10_ERROR_WRITEDB,					/*	filename			*/
	EASYH10_ERROR_UPDATEDB,					/*	NULL				*/

	EASYH10_ERROR_TUNEROPEN,
	EASYH10_ERROR_TUNERLOCALOPEN,
	EASYH10_ERROR_TUNERIMPORT,
	EASYH10_ERROR_TUNEREXPORT,
	EASYH10_ERROR_TUNERMERGE,

	EASYH10_WARNING_PLAYLIST_READ,			/*	src_filename		*/
	EASYH10_WARNING_PLAYLIST_CONVERT,		/*	src_filename		*/
	EASYH10_WARNING_PLAYLIST_CONVERT_ENTRY,	/*	music_filename		*/
	EASYH10_WARNING_PLAYLIST_WRITE,			/*	dst_filename		*/

	EASYH10_WARNING_TUNER_TOOMANYENTRIES,	/* NULL */
	EASYH10_WARNING_TUNER_INVALIDLINE,		/* line */
	EASYH10_WARNING_TUNER_INVALIDFREQUENCY,	/* line */
};


typedef int (*update_progress_callback)(void *instance, int phase, int param_int, void *param_ptr);

typedef void (*easyh10_error_callback)(void *instance, int code, void *param);

struct tag_easyh10_modelenv {
	const ucs2_char_t* system_location;
	const ucs2_char_t* database_location;
	const ucs2_char_t* music_location;
	const ucs2_char_t* playlist_location;
	const ucs2_char_t* playlist_extension;
};
typedef struct tag_easyh10_modelenv easyh10_modelenv_t;

int easyh10_get_modelenv(
	easyh10_modelenv_t* env,
	const ucs2_char_t* model_filename,
	void *instance,
	update_progress_callback progress_proc,
	easyh10_error_callback error_proc
	);

int easyh10_database(
	const ucs2_char_t* path_to_root,
	const ucs2_char_t* path_to_db,
	const ucs2_char_t* path_to_music,
	const ucs2_char_t* model_filename,
	const char *info_extractor,
	int flags,
	void *instance,
	update_progress_callback progress_proc,
	easyh10_error_callback error_proc
	);

int easyh10_playlist(
 	const ucs2_char_t* path_to_root,
	const ucs2_char_t* path_to_music,
	const ucs2_char_t* path_to_playlist,
	const ucs2_char_t* playlist_extension,
	int flags,
	void *instance,
	update_progress_callback progress_proc,
	easyh10_error_callback error_proc
	);

int easyh10_generate_template(
	const ucs2_char_t* path_to_db,
	const ucs2_char_t* filename,
	int flags,
	uint16_t fw_major_min,
	uint16_t fw_minor_min,
	uint16_t fw_major_max,
	uint16_t fw_minor_max,
	void *instance,
	update_progress_callback progress_proc,
	easyh10_error_callback error_proc
	);

int easyh10_dump(
	const ucs2_char_t *path_to_db,
	FILE *fp,
	const ucs2_char_t *model_filename,
	int flags
	);

int
easyh10_tuner_import(
	const ucs2_char_t* path_to_system,
	const ucs2_char_t* localfile,
	int flag,
	void *instance,
	update_progress_callback progress_proc,
	easyh10_error_callback error_proc
	);

int
easyh10_tuner_export(
	const ucs2_char_t* path_to_system,
	const ucs2_char_t* localfile,
	int flag,
	void *instance,
	update_progress_callback progress_proc,
	easyh10_error_callback error_proc
	);

#ifdef	__cplusplus
}
#endif/*__cplusplus*/

#endif/*__EASYH10_H__*/
